# Must be tool for configuration checking. Interestingly, if this script isn't
# a tool, it can mess with the BaseProp's configuration warnings
@tool
class_name Prop
extends BaseProp

# TODO: How do we handle a prop's traits? i.e. how do we classify props
# into food, weapons, etc. Materials? Composition? Can we use bit flags? Physics
# layers? Group tags?

#
# Description
#
## A physics object that can be grabbed, drop, thrown, and more
##
## The Prop is meant to be any of the inert objects that make up the world,
## react to the world using Godot's built-in physics, and are interacted with
## on a regular basis.
## [br][br]
## Each prop has two states: either [i]Physical[/i] or [i]Visual[/i]. In
## [i]Physical[/i] mode, the prop can be detected and reacts to physics - in
## other words, it behaves just as you would expect a physics object to behave.
## [i]Physical[/i] mode is intended for when an object has been dropped and is
## free-floating out in the world.
## [br][br]
## In [i]Visual[/i] mode, the prop becomes static and no longer collides with
## anything. This is the intended mode for when an prop is being held, equipped,
## or otherwise stored. When an prop switches to [i]Visual[/i] mode, it's layer
## and mask collisions are stored - they are restored once the prop re-enters
## [i]Physical[/i] mode.
## [br][br]
## The prop always starts in [i]Physical[/i] mode.
## [br][br]
## One other prop of note - the closest thing [i]Godot[/i] has to some sort of
## data tags or tagging system is the [i]group[/i] system. Because of this, what
## an actor is able to do with a prop is determined by it's relationships with
## the prop's different groups. This is currently being fleshed out and will
## be subject to change.

#
# Signal Declarations
#

#
# Enum Declarations
#
## Props in this game have two states. The enumeration will help us keep these
## different modes straight.
enum PropState {
    PROP_PHYSICAL,  ## Prop moves about & collides like a regular physics object
    PROP_VISUAL,  ## Prop doesn't collide with anything, lacks gravity.
}

#
# Constant Declarations
#

#
# Export Variables
#
## The prop's display name. This is the prop's identifier, from the user's
## perspective.
@export var display_name: String

## The prop's unique name key. This is the key for identifying props of a type.
## In other words, this is the value that will be compared to see if two props
## are, in fact the same type of prop.
## [br][br]
## Since this is used to check prop equivalence: please, for the love of god,
## do not change this at run time. I guess you could but I'm asking very nicely
## that you DO NOT!
@export var unique_key: String

## The max count of these props that may be stacked up for carrying purposes
@export_range(1, 1024) var max_carry_stack = 1

@export_group("Stack Options", "stack_")

## When this prop is in a stack, this is the initial offset of the stack's
## first item from the stack's origin. This is required because, otherwise, the
## prop could clip into whatever is holding the prop.
@export var stack_initial_offset: Vector3 = Vector3.ZERO

## When multiple props are stored in a stack, this is the measure of space
## between props. Mores specifically, it's the origin-to-origin offset between
## an item in the stack and it's neighbors to either the top or bottom.
@export var stack_per_prop_offset: Vector3 = Vector3.ZERO

## When a prop enters a stack, it is forced into this rotation. This is so we
## can anticipate and deal with possible clipping incidents in the stack.
## Important for props that may have odd sizes. Angles are in degrees.
@export var stack_rotation: Vector3 = Vector3(0, 45, 0)

#
# Public Variables
#
## The prop's current state is tracked by this var. We start in the physical
## mode. Setting this variable will update the state appropriately - however,
## for explicitness and clarity, calling [method to_physical_prop] and
## [method to_visual_prop] is the preferred way of updating this variable,
## rather than changing it directly.
var prop_state = PropState.PROP_PHYSICAL:
    set(new_state):
        prop_state = new_state
        if prop_state == PropState.PROP_PHYSICAL:
            _physical_prop_transition()
        elif prop_state == PropState.PROP_VISUAL:
            _visual_prop_transition()

#
# Private Variables
#
# When an prop switches to the visual only mode, it loses any-and-all collision.
# This ensures the object does not collide with anything, and nothing collides
# with it. However, we want to respect the collision configuration that
# previously existed. So we cache the layer and mask values into these two
# variables.
var _layer_cache
var _mask_cache


#
# Public functions
#
## Set this prop to a purely VISUAL (non-physical) state. It will not be
## affected by gravity, will not collide with anything, and nothing will collide
## with it. Returns the now visualized prop (which will just be the same
## object).
## [br][br]
## This should, generally, only be invoked by nodes that are managing
## the props.
func to_visual_prop() -> Prop:
    prop_state = PropState.PROP_VISUAL
    return self


## Set this prop to a purely VISUAL (non-physical) state. It will not be
## affected by gravity, will not collide with anything, and nothing will collide
## with it. Returns the now visualized prop (which will just be the same
## object).
## [br][br]
## This should, generally, only be invoked by nodes that are managing the props.
## Note that this doesn't remove the prop from whatever circumstances it was
## in - i.e. whether it was held, or stacked, or contained, etc.
func to_physical_prop() -> Prop:
    prop_state = PropState.PROP_PHYSICAL
    return self


#
# Private functions
#
#
# Internal function for transitioning into a visual state
func _visual_prop_transition() -> void:
    # Freeze the body, which kills the physics
    self.freeze = true

    # Capture the collision layer and the collision mask
    _layer_cache = self.collision_layer
    _mask_cache = self.collision_mask

    # Clear the collision layer and mask
    self.collision_layer = 0
    self.collision_mask = 0


# Internal function for transitioning into a physical state
func _physical_prop_transition() -> void:
    # Reassert the collision layer and mask
    self.collision_layer = _layer_cache
    self.collision_mask = _mask_cache

    # Unfreeze the body, which starts the physics again
    self.freeze = false

    # Chances are, while we were set to visual mode, we were sleeping. Need to
    # assert that this is no longer true
    self.sleeping = false

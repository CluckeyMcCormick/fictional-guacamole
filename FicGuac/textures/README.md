# Textures
This is the directory that contains (most) of our texture assets. Most of these assets were designed with a particular ratio in mind - 32 pixels corresponding to 1 world unit.

While we prefer to keep textures with the scenes that use them, the *Qodot* addon and *TrenchBroom* level editor require a `textures` directory. Ergo, this is where most of the textures that get used in our various Qodot scenes and assets are stored. 

## Building
Textures for our different buildings. These textures have been sorted into a series of "building materials". Each series is numbered, like `bm_s00` or `bm_s01`. This was done to group assets by their license. You should be aware, then, that not all assets in this directory exist under the project's license. So more restrictive licensing may be present - look out for `.license` files.

## Items
Textures for different 3D items - although it was mostly designed for world/level geometry, *TrenchBroom* is also pretty useful for modeling low-poly items. I've really struggled with other tools, especially *Blender*, so *TrenchBroom* is a welcome tool.

Like the building textures, these textures have been sorted into a series of "item materials". Each series is numbered, like `im_s00` or `im_s01`. This was done to group assets by their license. You should be aware, then, that not all assets in this directory exist under the project's license. So more restrictive licensing may be present - look out for `.license` files.

## Qodot
*Qodot* came with it's own set of sample textures, some of which seemed pretty useful to me. I decided to keep them around just in case I decided to use them.

## Terrain
The different Terrain textures go in this directory. 

## Util
These different textures generally serve some sort of utility role - measuring out world units, denoting special kinds of *TrenchBroom* brushes, etc.

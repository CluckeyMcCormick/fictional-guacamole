@tool
extends Node3D

## Core camera scene used for consistent control & display
##
## This is the standardized camera meant for use in game scenes and tests. It
## handles movement and zoom. More importantly, it actually consists of multiple
## cameras, with movement and resizes synced between them (that's the
## [i]rig[/i] part). These alternate cameras are then largely used for shaders
## and other special effects.
## [br][br]
## Each one of the cameras is already configured to Orthographic mode and is
## rotated appropriately for an isometric perspective. Because of that, the
## Camera Rig should never be rotated. It only needs to be moved into an
## initial position on x, y, and z.
## [br][br]
## Because camera movement is a bit complicated, there are a lot of different
## configurable options for the Camera Rig. For orthographic cameras - which
## all of our cameras in this rig ARE - what we would term the [i]zoom[/i] or
## possibly [i]FOV[/i] is controlled by the [member Camera3D.size] variable.
## Keep that in mind when looking at the configurables.
## [br][br]
## The camera has built-in controls for moving and zooming. These actions have
## several associated configurables, including disabling both zoom and
## movement.

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Variable Declarations
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## The basic, unmodified move rate for the camera - expressed in units/sec.
## Used to calculate our movement vector.
@export var basic_move_rate = 100:
    set(new_rate):
        # Set the basic move rate.
        basic_move_rate = clamp(new_rate, 5, INF)
        # Do all the necessary move rate updates
        _update_move_rate()

## For orthographic cameras - which all of our cameras in this rig ARE - what
## we would term the "zoom" or possibly "FOV" is controlled by the "size"
## variable. This configurable controls the current size of all cameras in the
## rig. This value will be updated as the size changes.
@export var camera_size: int = 150:
    set(new_size):
        camera_size = new_size
        # Set the camera size appropriately
        camera_size = clamp(new_size, min_camera_size, max_camera_size)
        # Update the camera to match these new constraints
        _update_cameras()
        # If we're in the editor, update those visual values
        if Engine.is_editor_hint():
            update_configuration_warnings()

## The maximum allowable camera size.
@export var max_camera_size: int = 150:
    set(new_size):
        max_camera_size = new_size
        # If we're in the editor, update those visual values
        if Engine.is_editor_hint():
            update_configuration_warnings()

## The minimum allowable camera size.
@export var min_camera_size: int = 15:
    set(new_size):
        min_camera_size = new_size
        # If we're in the editor, update those visual values
        if Engine.is_editor_hint():
            update_configuration_warnings()

## Every time we zoom out, we increase the camera's size; every time we zoom in
## we decrease the camera's size. This configurable controls the step the zoom
## increases/decreases every time - lower numbers means a more finely-grained
## but also slower zoom.
@export var zoom_step: int = 15:
    set(new_step):
        zoom_step = new_step
        if Engine.is_editor_hint():
            update_configuration_warnings()

## This configurable controls whether the camera can move (based on input). If
## a static camera is required, turn this configurable off.
@export var move_enabled: bool = true

## This configurable controls whether the camera can zoom (based on input). If
## a static camera is required, turn this configurable off.
## [br][br]
## Keep in mind that this only disables camera zoom from user input - the
## Camera Size can still be manipulated directly.
@export var zoom_enabled: bool = true

## If a value for a given axis is > 0, then the camera movement will be clamped
## on that axis. The camera is clamped to it's starting point on the axis,
## plus-or-minus the configured axis value.
## [br][br]
## In this instance, [b]the value for Y is treated as the Z axis[/b]. That's
## just a side effect of using a [Vector2].
@export var move_clamping_extents: Vector2 = Vector2(-1, -1)

# To move forward, backward, left and right, we need to translate the camera -
# which we can do with these vectors. Neat!
var _move_vector_FB = Vector3.ZERO
var _move_vector_LR = Vector3.ZERO

# When we recenter the camera, where does the camera move to? We'll have it
# reset to it's starting position, so we'll grab that
@onready var _recenter_point = self.position


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Signals
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# When the root viewport changes, our viewports on the camera rig don't change
# with it! We need to size up those viewports appropriately! We'll connect this
# function with the root viewport's resize event, and resize as appropriate.
func _on_root_viewport_size_changed():
    # First, get the root viewport
    var root_viewport = get_tree().get_root()
    # Now, trickle those sizes down to our rig's sub-viewports
    $XrayWorld.size = root_viewport.size
    $SilhouetteWorld.size = root_viewport.size


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Camera3D Rig specific functions and utilities
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# Updates all our cameras to be the same size. Then, recalculate the move rate
# using the new camera size.
func _update_cameras() -> void:
    # Assert camera size for the core camera
    if has_node("CoreCamera"):
        $CoreCamera.size = camera_size
    # Assert camera size and position for all other nodes
    if has_node("XrayWorld/Camera3D"):
        $XrayWorld/Camera3D.size = camera_size
    if has_node("SilhouetteWorld/Camera3D"):
        $SilhouetteWorld/Camera3D.size = camera_size
    # Since the move rate derives from the camera size, we need to update the
    # move rate as well
    _update_move_rate()


# Updates the camera move rate proportional to our total zoom
func _update_move_rate() -> void:
    # First, update our move vectors. Start by creating simple vectors in the
    # correct directions.
    _move_vector_FB = Vector3(-1, 0, -1)
    _move_vector_LR = Vector3(1, 0, 1).rotated(Vector3(0, 1, 0), PI / 2)
    # Next, calculate the current move rate. This is just the basic move rate
    # scaled by the camera size.
    var curr_move_rate = (
        basic_move_rate * (camera_size / float(max_camera_size))
    )
    # Now, we normalize and scale by the basic move rate. That creates move
    # vectors with the appropriate lengths.
    _move_vector_FB = _move_vector_FB.normalized() * curr_move_rate
    _move_vector_LR = _move_vector_LR.normalized() * curr_move_rate


## Returns the viewport texture for the XRay World, which does not render
## "obstacle actual" but instead renders the "obstacle alternate" world. In
## effect, this texture is always looking "behind" walls. This makes it great
## for shaders that need to see-through walls.
func get_xray_texture() -> ViewportTexture:
    return $XrayWorld.get_texture()


## Returns the viewport texture for Silhouette World, which only renders those
## items that exist on the "Silhouettable" visual layer. When used
## appropriately, this is perfect for shaders that render silhouettes from
## behind walls.
func get_silhouette_texture() -> ViewportTexture:
    return $SilhouetteWorld.get_texture()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Godot Processing - _ready, _process, etc.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# Called when the node enters the scene tree for the first time.
func _ready():
    # Assert Status
    _update_cameras()

    # If we're in the editor, back out. We don't want to mess with this next
    # step.
    if Engine.is_editor_hint():
        return

    # When the root viewport changes, our viewports on the camera rig don't
    # change with it! We need to size up those viewports appropriately! We'll
    # connect a function with the root viewport's resize event, and resize as
    # appropriate.
    get_tree().root.size_changed.connect(_on_root_viewport_size_changed)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
    # If we're in the editor, or movement is disabled, back out!
    if Engine.is_editor_hint() or not move_enabled:
        return
    # We moved THIIIIS much!
    var translator = Vector3.ZERO

    # Handle our camera movement
    if Input.is_action_pressed("camera_move_forward"):
        translator += delta * _move_vector_FB
    if Input.is_action_pressed("camera_move_backward"):
        translator -= delta * _move_vector_FB
    if Input.is_action_pressed("camera_move_right"):
        translator += delta * _move_vector_LR
    if Input.is_action_pressed("camera_move_left"):
        translator -= delta * _move_vector_LR

    self.translate(translator)

    if move_clamping_extents.x >= 0:
        position.x = clamp(
            position.x,
            _recenter_point.x - move_clamping_extents.x,
            _recenter_point.x + move_clamping_extents.x
        )
    if move_clamping_extents.y >= 0:
        position.z = clamp(
            position.z,
            _recenter_point.z - move_clamping_extents.y,
            _recenter_point.z + move_clamping_extents.y
        )


# Process an input event. Intended for single-press input events, like the
# camera
func _input(event):
    # If we're in the editor, or zoom is disabled, back out!
    if Engine.is_editor_hint() or not zoom_enabled:
        return

    # Did we zoom in-or-out? If so, we need to update the cameras!
    var zoom_change = false

    if event.is_action_pressed("camera_recenter"):
        self.position = _recenter_point

    if event.is_action_pressed("camera_zoom_in"):
        camera_size = clamp(
            camera_size - zoom_step, min_camera_size, max_camera_size
        )
        zoom_change = true

    if event.is_action_pressed("camera_zoom_out"):
        camera_size = clamp(
            camera_size + zoom_step, min_camera_size, max_camera_size
        )
        zoom_change = true

    if event.is_action_pressed("debug_print"):
        print(self.size)

    # If we zoomed in or out, we need to change the cameras.
    if zoom_change:
        _update_cameras()


# This function is very ugly, but it serves a very specific purpose: it allows
# us to generate warnings in the editor in case the CameraRig is misconfigured.
func _get_configuration_warnings():
    # Array of warnings
    var warnings = []

    # Test 1: are the constraints sane?
    if not min_camera_size < max_camera_size:
        warnings.append("Minium camera size exceeds or meets max camera size!")

    # Test 2: is the maximum camera size zero?
    if max_camera_size == 0:
        warnings.append("Maximum camera size of 0 WILL CRASH!")

    # Test 3: is the minimum camera size at or below zero?
    if min_camera_size <= 0:
        warnings.append(
            "Minimum camera size is <= 0! This can cause movement issues."
        )

    # Test 4: is the camera size actually in range?
    if camera_size != clamp(camera_size, min_camera_size, max_camera_size):
        warnings.append("Camera size outside configured range!")

    # Test 5: Is zoom step negative?
    if zoom_step < 0:
        warnings.append(
            "Zoom step is negative! Control behavior will be inverted!"
        )

    # Test 6: Is zoom step zero?
    if zoom_step == 0:
        warnings.append("Zoom step is zero! Zoom will not function!\n")

    return warnings

# Scripts
This directory is where utility scripts are stored - these utility scripts can
be likened to tools. Unlike most scripts, they are not attached to a particular
scene and are meant to be manually loaded into whatever class requires them.

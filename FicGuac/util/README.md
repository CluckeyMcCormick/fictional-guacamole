# Util
This directory holds different assets (and the occasional scene) that aren't
exactly game-ready, but don't belong exclusively in the tests directory.
These are typically assets used for debugging or prototyping.

Admittedly, it's also somewhat of a dumping ground for assets that don't fit
neatly into any other category.

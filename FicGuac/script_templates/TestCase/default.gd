extends _BASE_

#------------------------------------------
# Test setup
#------------------------------------------


# Called once per test case, before any test
# gdlint:ignore = function-name
static func beforeAll() -> void:
    pass


# Called once per test case, after any test
# gdlint:ignore = function-name
static func afterAll() -> void:
    pass


# Called before each test
# gdlint:ignore = function-name
func beforeEach() -> void:
    pass


# Called after each test
# gdlint:ignore = function-name
func afterEach() -> void:
    pass


#------------------------------------------
# Tests
#------------------------------------------


func test() -> void:
    pass

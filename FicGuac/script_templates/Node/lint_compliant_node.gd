# meta-name: GDLint Compliant Node
# meta-description: Script outlining the GDLint-compliant order of declarations
# meta-default: true
# meta-space-indent: 4
# @tool
# class_name ???
extends Node

#
# Description
#
## Concise script/node summary
##
## A more detailed description of this node that can span multiple paragraphs.
##
## @tutorial(Godot Docs): docs.godotengine.org/en/stable

#
# Signal Declarations
#
# signal sample_signal(value)

#
# Enum Declarations
#
# enum ExampleEnum { A = 0, B = 3.5, }

#
# Constant Declarations
#
# const VALUE = 5

#
# Export Variables
#
# @export var export_value : String = ""

#
# Public Variables
#
# var public_var : int = 0

#
# Private Variables
#
# var _private_var : bool = false

#
# Public onready Variables
#
# @onready var also_public_var : int = getSomething()

#
# Private onready Variables
#
# @onready var _also_private_var : float = getSomethingPrivate()

#
# Public functions
#
# func something():
#     pass

#
# Private functions
#


# Called when the node enters the scene tree for the first time.
func _ready():
    pass  # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
    pass

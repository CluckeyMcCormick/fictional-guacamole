extends MeshInstance3D

## A mesh that can appear/disappear depending on whether it can be seen
##
## The [i]Hiding Mesh[/i] is a mesh that shows and hides itself depending on
## whether it can be "seen" by the camera. Rather than using a
## [VisibleOnScreenNotifier3D] node to track this, the [i]Hiding Mesh[/i] uses
## physics raycasts to a dedicated camera node. This is a bit messy since it
## meshes visual and physical components but it's the best way to track
## occlusion (as opposed to whatever heuristic [VisibleOnScreenNotifier3D]
## uses).
## [br][br]
## The default behavior of the mesh is to show when hidden - that is, it will
## show itself when occluded. This behavior can be inverted, so that it only
## shows when showing (and hides when hidden).
## [br][br]
## There is an option to show or hide the [i]Hiding Mesh[/i] with an animation
## instead of just changing the visibility.

## This option, enabled by default, controls when the shows itself. If enabled,
## the mesh only shows itself when hidden/occluded from view. It will hide when
## it is no longer occluded.
##
## Disabling this option inverts the behavior: the mesh shows itself when not
## occluded, and hides when occluded.
@export var show_on_hidden: bool = true
# Is this mesh currently hidden from the camera?
var _cam_hide

## In order to test whether the mesh is occluded or not, we need a target node
## to cast to. This is intended to be a [Camera3D] node, but any [Node3D] will
## suffice. This is only resolved at runtime.
##
## If this node is not provided, the [i]Hiding Mesh[/i] will not be able to
## test occlusion and the hiding functionality will not work.
@export_node_path("Node3D") var target_camera: NodePath = ^""

## By default, we cast from the mesh's origin to the [member
## target_camera_node]'s origin. However - sometimes we might, for whatever
## reason, want to cast from a different node's origin rather than the mesh's
## origin.
##
## This configurable node, if specified, will serve as the occlusion raycast's
## origin.
@export_node_path("Node3D") var test_source: NodePath = ^""

## This scene uses physics raycasts to determine occlusion. This physics layer
## configurable allows for fine control of what physics layers will block the
## raycast and can thus be considered [i]occluding[/i].
@export_flags_3d_physics var occlusion_layers = 4

## This configurable controls the length of the mesh's grow-and-shrink
## animation. If this value is less than or equal to 0, there will be no
## animation (this is the default). Instead, the visibility of the Hiding Mesh
## will be toggled appropriately.
@export var animation_length: float = -1

## This enum is for purely internal use, just to track our current state. This
## really only used when the hiding mesh is in animation mode.
enum { SHRINKING, GROWING, HIDDEN, SHOWING }
# Variable for tracking the above states.
var _hide_state = SHOWING

# The tween object we use for inflating/deflating this mesh
var _tween: Tween = null


func _ready():
    # By default, set the HidingMesh to visible. This will help load the frame
    # dips up front.
    self.visible = true


func _physics_process(delta):
    # Resolve the camera and source nodes.
    var target_camera_node = get_node_or_null(target_camera)
    var test_source_node = get_node_or_null(test_source)

    # If we don't have a camera to raycast to, BACK OUT!
    if target_camera_node == null:
        return

    # If we don't have a source node, use ourselves.
    if test_source_node == null:
        return

    # First, let's get the space state.
    var space_state = get_world_3d().direct_space_state
    var rayParam = PhysicsRayQueryParameters3D.create(
        test_source_node.global_transform.origin,  # Ray origin
        target_camera_node.global_transform.origin,  # Ray destination
        occlusion_layers,  # Collision MASK - what to collide with
        []  # Node exclusion list; we exlude nothing!
    )
    # Now, cast from this mesh to that camera
    var result = space_state.intersect_ray(rayParam)

    # Evaluate: did we get a result back? If we did, then the camera is hidden.
    # We'll stick the evaluation in our camera_hidden variable.
    _cam_hide = not result.is_empty()

    # If we have to show ourselves...
    if (show_on_hidden and _cam_hide) or (not show_on_hidden and not _cam_hide):
        # And we aren't already grown or growing...
        if _hide_state == SHRINKING or _hide_state == HIDDEN:
            # Then show the mesh. Or grow the mesh. One of the two, this
            # function will handle it.
            _start_showing_process()
    # Otherwise, we need to hide ourselves...
    else:
        # BUT, we should only hide ourselves if we're already showing.
        if _hide_state == GROWING or _hide_state == SHOWING:
            # Then hide the mesh. Or shrink the mesh. One of the two, this
            # function will handle it.
            _start_hiding_process()


func _start_showing_process():
    # Need to make sure we're visible.
    self.visible = true

    # If we don't have an animation length...
    if animation_length <= 0:
        # Assert scale (just in case)
        self.scale = Vector3(1, 1, 1)
        # Current state is now showing
        _hide_state = SHOWING
        # and back out
        return

    # Otherwise, we're definitely going to animate. The state is now GROWING
    _hide_state = GROWING

    if _tween != null:
        _tween.kill()
        _tween = null

    # Make the tween
    _tween = create_tween()

    # Set the tween up to grow the mesh
    (
        _tween
        . tween_property(
            self,  # Target node
            "scale",  # Property to tween
            Vector3(1, 1, 1),  # Target value - we grow to default size
            animation_length,  # Time period - configurable!
        )
        . set_trans(Tween.TRANS_LINEAR)
        . set_ease(Tween.EASE_IN_OUT)
    )

    # Connect the "on completed" tween
    _tween.finished.connect(_on_tween_completed)

    # Start that tween!
    _tween.play()


func _start_hiding_process():
    # If we don't have an animation length...
    if animation_length <= 0:
        # Just hide the thing then, I guess.
        self.visible = false
        # Current state is now hidden
        _hide_state = HIDDEN
        # Back out.
        return

    # Otherwise, we're definitely going to animate. The state is now SHRINKING
    _hide_state = SHRINKING

    # Make the tween
    if _tween != null:
        _tween.kill()
        _tween = null

    # Make the tween
    _tween = create_tween()

    # Set the tween up to grow the mesh
    (
        _tween
        . tween_property(
            self,  # Target node
            "scale",  # Property to tween
            Vector3(0, 0, 0),  # Target value - shrink to nothing
            animation_length,  # Time period - configurable!
        )
        . set_trans(Tween.TRANS_LINEAR)
        . set_ease(Tween.EASE_IN_OUT)
    )

    # Connect the "on completed" tween
    _tween.finished.connect(_on_tween_completed)

    # Start that tween!
    _tween.play()


func _on_tween_completed(object, key):
    # If we were growing, we're now showing
    if _hide_state == GROWING:
        _hide_state = SHOWING
        #print("Grown!")

    # If we were shrinking, we're now hidden
    elif _hide_state == SHRINKING:
        _hide_state = HIDDEN
        #print("Shrank!")
        # Hide the mesh, to save processing
        self.visible = false

    _tween = null


# This function is very ugly, but it serves a very specific purpose: it allows
# us to generate warnings in the editor in case the HidingMesh is misconfigured.
func _get_configuration_warnings():
    # (W)a(RN)ing (STR)ing
    var wrnstr = ""

    # Test 1: Did we actually get handed a camera node?
    if target_camera.is_empty():
        wrnstr += "User MUST provide a Target Camera3D node!\n"

    # Test 2: is the camera node real? Does it exist?
    if get_node(target_camera) == null:
        wrnstr += "Provided Target Camera3D node doesn't exist!\n"

    # Test 3: is the camera node the right type? It doesn't have to be
    if typeof(get_node(target_camera)) != typeof(Camera3D):
        wrnstr += "Target Camera3D is not actually a camera!\n"
        wrnstr += "It NEEDS to be a spatial, but it SHOULD be a Camera3D!\n"

    return wrnstr

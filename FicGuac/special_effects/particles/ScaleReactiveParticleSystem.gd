extends Node3D

## A base scene for particle systems that react to scale changes
##
## In Godot 4, scaling a particle system that uses a [ParticleProcessMaterial]
## will scale up the size of the emitter while leaving the scale of the
## individual particles intact. This is actually good - it's the exact behavior
## we want.
## [br][br]
## However, sometimes the particles don't look good when the scale up. Maybe we
## actually do want the particle size to scale up. Or maybe we want to increase
## the particle count to maintain a particle density.
## [br][br]
## This scene is meant to be the basis for all of our particles system which
## react to scaling. Which is meant to be all of them.
## [br][br]
## Note that, as a general rule of thumb, we assume the default size of the
## particle system to be a 1x1x1 meter cube. This makes scaling to match an
## entity much easier.


func _ready():
    # Enable local transform notification - that way we get notified whenever
    # the Particle System is moved locally or scaled locally. We don't care as
    # much about the global transform.
    set_notify_local_transform(true)

    # Now that we've entered the scene, assert the scale. This will force the
    # system to use a default value
    assert_new_scale()


func _notification(what):
    # If this is some notification NOT related to the local transform, back out!
    if what != NOTIFICATION_LOCAL_TRANSFORM_CHANGED:
        return
    # Assert the new scale
    assert_new_scale()


## Virtual function, designed to be overwritten. Called whenever the scale
## changes.
func assert_new_scale():
    pass

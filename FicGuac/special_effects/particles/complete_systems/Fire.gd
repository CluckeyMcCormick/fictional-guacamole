extends "res://special_effects/particles/ScaleReactiveParticleSystem.gd"

## A particle system for fire with dense smoke.
##
## A scale reactive particle system for fire with dense smoke. Since it's scale
## reactive, this can be used for just about any need for something to be on
## fire. Consider it the most basic variation of the concept of "fire".
## [br][br]
## This system assumes it's default dimensions to be 1x1x1. The number of
## particles scales with the volume, according to base values and linear slopes
## given by the configurables.
## [br][br]
## The relationship between the slope and the volume is hard to categorize,
## since volume is inherently a cubic value. To make the volume behave in a more
## linear fashion, we take the cubic root and THEN multiply it by the slope.

@export_group("Fire Particle System", "fire_")
## This is the base particle count for the fire system at a scale of 1x1x1.
@export var fire_base_particle_count: int = 10
## This is the slope for the fire system. The number of fire particles will
## increase by this amount whenever the volume increases by 1. That's a rough
## approximation, but it holds true.
@export var fire_root_particle_slope: float = 30.0

@export_group("Smoke Particle System", "smoke_")
## This is the base particle count for the fire system at a scale of 1x1x1.
@export var smoke_base_particle_count = 10
## This is the slope for the smoke system. The number of smoke particles will
## increase by this amount whenever the volume increases by 1. That's a rough
## approximation, but it holds true.
@export var smoke_root_particle_slope = 30.0


## Override of the [i]ScaleReactiveParticleSystem[/i]'s virtual function.
## Updates the particle count according to the current volume of the system:
## We assume the volume to be given by 1x1x1 at a scale of (1,1,1), so we can
## just multiply the scale together to get what we need.
func assert_new_scale():
    # Calculate the volume - this is the formula for a standard cube/box
    var new_volume = abs(self.scale.x) * abs(self.scale.y) * abs(self.scale.z)
    # The amount of particles to spawn is is our base...
    var fire_amount = self.fire_base_particle_count
    var smoke_amount = self.smoke_base_particle_count
    # ... PLUS the cubic root of the volume * the density, MINUS the slope.
    # See, at a volume of 1, the result of this formula should add 0. That
    # way, we subtract particles when under 1, and add particles when over
    # 1.
    fire_amount += (
        roundi(pow(new_volume, 1.0 / 3.0) * self.fire_root_particle_slope)
        - roundi(self.fire_root_particle_slope)
    )
    smoke_amount += (
        roundi(pow(new_volume, 1.0 / 3.0) * self.smoke_root_particle_slope)
        - roundi(self.smoke_root_particle_slope)
    )

    # Update the particle count for the Fire & Smoke emitters
    $Fire.amount = fire_amount
    $Smoke.amount = smoke_amount

extends "res://special_effects/particles/ScaleReactiveParticleSystem.gd"

@export_category("Poison Bubbles Particle System")
## This is the base particle count for the poison bubble system at a scale of
## 1x1x1.
@export var base_particle_count: int = 10
## This is the slope for the bubble system. The number of bubble particles will
## increase by this amount whenever the volume increases by 1. That's a rough
## approximation, but it holds true.
@export var root_particle_slope: float = 30.0


## Override of the [i]ScaleReactiveParticleSystem[/i]'s virtual function.
## Updates the particle count according to the current volume of the system:
## We assume the volume to be given by 1x1x1 at a scale of (1,1,1), so we can
## just multiply the scale together to get what we need.
func assert_new_scale():
    # Calculate the volume - this is the formula for a standard cube/box
    var new_volume = self.scale.x * self.scale.y * self.scale.z
    # The amount of particles to spawn is is our base...
    $Bubbles.amount = self.base_particle_count
    # ... PLUS the cubic root of the volume * the density, MINUS the slope.
    # See, at a volume of 1, the result of this formula should add 0. That
    # way, we subtract particles when under 1, and add particles when over
    # 1.
    $Bubbles.amount += (
        roundi(pow(new_volume, 1.0 / 3.0) * self.fire_root_particle_slope)
        - roundi(self.fire_root_particle_slope)
    )

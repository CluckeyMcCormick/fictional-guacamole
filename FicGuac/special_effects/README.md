# Special Effects
The *Special Effects* directory is for any assets (shaders in particular) and
scenes that are used to create visual effects, but don't really have any other
purpose outside of that.

## Viewport Shader Templates
We have a special class of shaders, known as the *Viewport Shaders*, that use
`Viewport` nodes to create various effects. There are currently a lot of
variations on these shaders, and their use of `ViewportTexture` textures
means that they have behavior unique amongst other shaders. 

## Text Spritesheets
This directory stores our text spritesheets - we use these for non-gui text
elements. Stuff that exists out in the world.

## Particles
This directory contains the different `ParticlesMaterial` assets, and other
assets relating to those Materials - particle sprite textures, and the like.

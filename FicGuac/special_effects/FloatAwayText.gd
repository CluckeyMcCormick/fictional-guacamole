extends Label3D

## A scene for temporary text that floats straight upwards
##
## An extension of [Label3D]. When spawned, this text automatically floats
## upward, persisting for a fixed amount of time before disappearing. This is
## perfect for temporary text or notifications, such as damage counters.

@export_category("Float Away Text")

## The Float Away Text floats directly upwards once spawned. This controls the
## velocity, in units-per-second, that it moves upward. Note that making this
## negative causes it to float downward, and zero will make for no movement at
## all.
@export var float_velocity: float = 1.0

## This configurable controls how long the text exists for before it begins the
## disappearing animation. The total lifetime is given by this value
## [b]plus[/b] the fade time.
@export_range(0.01, 30, 0.01, "or_greater", "suffix:seconds")
var life_time: float = 1.5

## This configurable controls how long the disappearing animation lasts. The
## total lifetime is given by this value [b]plus[/b] the life time.
@export_range(0.01, 30, 0.01, "or_greater", "suffix:seconds")
var fade_time: float = .5


# Called when the node enters the scene tree for the first time.
func _ready():
    $LifeTimer.start(life_time)


func _physics_process(delta):
    # Move ourselves up by using our velocity and the time delta
    self.translate(Vector3(0, float_velocity * delta, 0))


func _on_LifeTimer_timeout():
    var tween: Tween = create_tween()
    # Shrink ourselves.
    (
        tween
        . tween_property(
            self,  # Object
            "scale",  # Property
            Vector3.ZERO,  # Final Value
            fade_time,  # Duration
        )
        . from_current()
        . set_trans(Tween.TRANS_SINE)
    )
    # Ensure we catch when the tween is done
    tween.finished.connect(_on_tween_all_completed)
    # Start the shrinking
    tween.play()


func _on_tween_all_completed():
    # Now that we've "disappeared", remove ourselves from the scene tree
    self.get_parent().remove_child(self)
    # Destroy ourselves
    self.queue_free()

extends ColorRect

## A simple pause menu, meant to be common to all active game scenes.
##
## The PauseMenu presents the user with a pretty basic pause menu - Resume,
## Main Menu, and Quit Game.
##
## When the user selects a particular option, a corresponding signal will be
## emitted - either one of [signal resume_game], [signal resume_game], or
## [signal exit_game].
##
## At this time, it doesn't handle any pause functionality in the same way the
## LoadingScreen does - i.e. by directly invoking
## [code]get_tree().paused = true[/code]. It could do this in the future, but
## for right now we're going to leave that the parent scene.
##
## One very important thing to note - this scene can only work because the
## root node's [code]process_mode[/code] is set to [b]Always[/b]. This ensures
## it regardless of whether the game is paused or not - VERY important for a
## pause menu!

## Signal emitted when the user selects the "resume game" menu option. Also
## emitted when the user attempts to exit the menu via controls.
signal resume_game

## Signal emitted when the user selects the "main menu" menu option.
signal main_menu

## Signal emitted when the user selects the "exit game" menu option. In this
## context, "exit game" means to quit the binary and return-to-desktop.
signal exit_game

## If true, the scene is effictively disabled: none of the buttons are
## functional and all user input is disabled. This is useful for preventing
## the user from "double selecting" an option or accidentally selecting items
## from an invisible menu.
var disabled: bool = true:
    set(new_state):
        disabled = new_state
        if disabled:
            %ResumeButton.disabled = true
            %MenuButton.disabled = true
            %ExitButton.disabled = true
        else:
            %ResumeButton.disabled = false
            %MenuButton.disabled = false
            %ExitButton.disabled = false


# Process input.
func _input(event):
    # If the pause menu is enabled/active and the user canceled...
    if not disabled and event.is_action_pressed("ui_cancel"):
        emit_signal("resume_game")
        # Handle this event so that it doesn't repeat
        get_viewport().set_input_as_handled()


func _on_resume_button_pressed():
    # Tell the world!
    emit_signal("resume_game")


func _on_menu_button_pressed():
    # Tell the world!
    emit_signal("main_menu")


func _on_exit_button_pressed():
    # Tell the world!
    emit_signal("exit_game")

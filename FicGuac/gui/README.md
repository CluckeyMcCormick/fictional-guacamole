# GUI
This directory contains our common in GUI elements. The plan for future scenes
is for them to handle pausing and loading at the scene level. We could have
multiple game scenes, or just one core game scene - it's really up in the air
right now. These common GUI scenes are provided for consistency between any
scene; now and future.

Note that the *Tests* currently use these scenes in a different way, see that
directory for more information.

In keeping with Godot's Scene Philosophy, each of these scenes is as self
contained as possible. It's up to external scenes to activate and deactivate
these GUI scenes (and listen to the signals emitted by these scenes).


extends ColorRect

## A loading screen GUI that can load a resource in the background
##
## The LoadingScreen presents the user with a loading animation, and a metric of
## progress. It loads a given resource in the background and sends out either
## the [signal loading_complete] or [signal loading_failed] signals depending
## on whether the load succeeded or failed. The [signal loading_complete] signal
## will emit the newly loaded resource.
##
## This scene was intended to background load large scenes - like a new stage
## level or a distinct test. It could be used for other scenes and resources,
## but that would likely be excessive. Using a combination of Godot's
## [method @GDScript.load] and [method @GDScript.preload] functions is generally
## recommended.
##
## This background loading works very well but be aware that loading the scene
## is different from spawning it in. You may experience freezing once you spawn
## in a resource loaded by this scene - correcting this is outside the scope of
## this scene.

# TODO: Move loading out of this scene and into another class - perhaps one that
# feeds directly into this scene.

# Are we currently loading something?
var _load_in_progress = false
# The current path we're trying to load.
var _current_load_path = null
# The tween we use for playing loading animations
var _load_tween = null

## Signal issued when loading is complete. Provides the resource loaded (should
## be a scene if you're using this right) and the path originally provided for
## loading.
signal loading_complete(resource, path)

## Signal issued when loading fails due to an error. Provides the resource path
## we were TRYING to load.
signal loading_failed(path)


func _process(_delta):
    # We'll write the Resource Loader's progress into this array.
    var progress = []

    # If we're not loading right now, back out!
    if not _load_in_progress:
        return

    # Otherwise, poll the loader.
    var status = ResourceLoader.load_threaded_get_status(
        _current_load_path, progress
    )

    match status:
        ResourceLoader.THREAD_LOAD_INVALID_RESOURCE, ResourceLoader.THREAD_LOAD_FAILED:
            # Stop the tween
            _load_tween.stop()
            # We're no longer trying to load anything
            _load_in_progress = false
            # Tell whoever is out there loading failed
            emit_signal("loading_failed", _current_load_path)

        ResourceLoader.THREAD_LOAD_LOADED:
            # Stop the tween
            _load_tween.stop()
            # We're no longer trying to load anything
            _load_in_progress = false
            # Get our resource
            var new_res = ResourceLoader.load_threaded_get(_current_load_path)
            # Tell whoever is out there loading failed
            emit_signal("loading_complete", new_res, _current_load_path)

        ResourceLoader.THREAD_LOAD_IN_PROGRESS:
            %ProgressBar.value = progress[0] * 100


## Starts loading the scene given by the path string. Does not return anything -
## instead, it will emit the "loading failed" signal if the load process
## couldn't be started.
func initiate_scene_load(scene_path_load):
    # Load the provided path
    var err = ResourceLoader.load_threaded_request(scene_path_load)

    # If we got an error, send a signal out - so whoever is out there knows
    if err != OK:  # Check for errors.
        emit_signal("loading_failed", scene_path_load)
        return

    # We're now loading
    _load_in_progress = true

    # This new path is now our current load path!
    _current_load_path = scene_path_load

    # Create an infinitely running tween, rotating our circle from 0 to 360
    # degrees
    _load_tween = create_tween().set_loops()
    _load_tween.tween_property(%Circle, "rotation_degrees", 360, 1).from(0)

    # Start it!
    _load_tween.play()


## Resets the loading screen so it can be reused (if we want to). This means
## resetting it visually and forgetting about any in progress loads.
func reset():
    # Reset the progress bar
    %ProgressBar.value = 0
    # Reset the ball
    %Circle.rotation_degrees = 0
    # If we have a tween, stop it
    if _load_tween != null:
        _load_tween.stop()
    # Reset our load values
    _load_in_progress = false
    _current_load_path = null
    _load_tween = null

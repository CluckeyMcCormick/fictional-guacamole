@icon("res://theater/icons/open-book.png")
class_name CommonStats
extends Resource

#
# Description
#
## A resource for all theater components to track common stats & status
##
## The Common Stats resource is the primary status component for our theater
## components. It defines the most basal properties that all components have in
## common.
## [br][br]
## This resource divides the stats into "base" stats - what the stats should be
## initially - and the "current" or "effective" status. The idea is that the
## base stats are fixed, and we can always derive calculations from these
## values. The curr/effective values are the values actually used in code, and
## are directly manipulated/affected by status conditions and other outside
## factors.

#
# Export Variables
#
## The base hitpoints for the object. This currently acts as both the default
## amount of health and a cap.
@export_range(1, INF, 1, "or_greater") var base_hp: int = 50

#
# Public Variables
#
## The effective hitpoint cap. Manipulating this health cap allows us to
## simulate temporary overheal buffs or health-limiting poisons. You can get
## the amount of over/underheal by calculating (eff_hp_cap - base_hp)
var eff_hp_cap: int = base_hp:
    set(new_val):
        eff_hp_cap = new_val
        # Clamp the effective base hitpoints. We have no upper limit (right
        # now)
        eff_hp_cap = clamp(eff_hp_cap, 1, INF)
        # If our current hitpoints exceeds our max allowance, lower the
        # available hitpoints
        if curr_hp > eff_hp_cap:
            curr_hp = eff_hp_cap

## The current hitpoints for this object.
var curr_hp: int = base_hp:
    set(new_val):
        curr_hp = new_val
        # Clamp the current hitpoints between 0 and the effective hitpoint cap
        curr_hp = clamp(curr_hp, 1, INF)

# TODO: Signals for variable updates? Could help with updating GUI elements...

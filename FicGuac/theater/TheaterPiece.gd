class_name TheaterPiece
extends Resource

#
# Description
#
## A common interface object for theater pieces
##
## When we do physics-driven interactions like raycasts, we get a physics
## object back. But the default physics objects - and there are many types-
## don't inherently support fields like "health" or ineractions like "damage".
## In fact, they don't even support the same physics methods, let alone
## standalone game concepts!
## [br][br]
## And not that they should! But it does leave us with a problem: if the engine
## hands us a physics object, how do we identify that physics object, and how
## do we interact with that object?
## [br][br]
## The [i]Theater Piece[/i] concept is an attempt to fix this issue. The idea
## is we'll have distinct subtypes/classes for all the necessary default
## physics objects. This object will standardize the properties, signals,
## functions, constants, and other fields that are required for a subtype/class
## to be considered a [i]Theater Piece[/i]. Using the properties system
## (GDScript's setters & getters), we can wrap the code found in this object.
## [br][br]
## This solves the issue of identification (we can simply check the type), as
## well as the issue of interaction (this interface provides for common
## fields). It also allows us to save some code by simply wrapping the
## functionality of this object.

#
# Signal Declarations
#
## Emitted when the theater piece enters the ALIVE life state (see
## [enum LifeState] and [member life_state]). Since ALIVE is the default state,
## and the life state is only supposed to procede linearly, it's unlikely this
## signal will fire unless [member allow_life_regression] is on and the
## [member life_state] is deliberately changed.
signal alive
## Emitted when the theater piece enters the DYING life state (see
## [enum LifeState] and [member life_state]). This state, and thus this signal,
## will be skipped if [member use_dying_state] is false.
signal dying
## Emitted when the theater piece enters the DEAD life state (see
## [enum LifeState] and [member life_state]).
signal dead
## Signal emitted whenever this node receives damage via [method damage]. Emits
## the amount of damage dealt as a positive integer. This is the actual damage
## received, after any necessary reductions.
signal damage_received(amount: int)
## Signal emitted whenever this node receives healing via [method heal]. Emits
## the amount of damage healed as a positive integer. This is the actual healing
## received, after any necessary reductions.
signal healing_received(amount: int)
## Signal emitted after [signal dead] IF [member remove_on_death] is enabled.
## This indicates that the corresponding theater component node should remove
## itself. Recommend against connecting to this signal outside of any base
## theater component.
signal death_cleanup
## Signal emitted whenever the theater piece takes damage or heals - so long as
## [member damage_float_text] or [member healing_float_text] is enabled. Emits
## the damage float node, which needs to be placed into a scene. Recommend
## against connecting to this signal outside of any base theater component. If
## a theater component needs to be notified of damage and healing, use the
## corresponding [signal damage_received] and [signal healing_received] signals.
signal float_away_text(text_node: Node)

#
# Enum Declarations
#
## A enumeration of the TheaterPiece's various life states. We use this to
## track the actor's current state. The order declared here is intended to be
## the progression of states. Going to an earlier state is considered a
## [i]regression[/i], and is not allowed unless [member allow_life_regression]
## is true.
enum LifeState {
    ALIVE = 0,  ## The actor is alive and moving around.
    DYING = 1,  ## The actor is dying, carrying out some pre-death actions.
    DEAD = 2,  ## The actor is officially dead.
}

#
# Constant Declarations
#
# This scene allows us to spawn little messages in the world, as needed. We'll
# use this to show damage values.
# gdlint:ignore = load-constant-name
const _FLOAT_AWAY_TEXT = preload("res://special_effects/FloatAwayText.tscn")

#
# Export Variables
#
## That stats-and-status resource for this theater piece. This is needed to
## ensure common stats-and-status (i.e. health) across all theater components.
## This is a sub-resource to allow for a possible variety of Stats resources -
## for example, one with fields that are unique entirely to Actors, or one
## with fields for a special variety of prop. Things of that nature.
@export var stats: CommonStats = null

## If set to true, we will fire the float_away_text signal whenever we take
## damage, with a new FloatAwayText node configured to show the damage dealt.
@export var damage_float_text: bool = true
## If set to true, we will fire the float_away_text signal whenever we receive
## healing, with a new FloatAwayText node configured to show the damage healed.
@export var healing_float_text: bool = true

@export_group("Life Progression")

## This property controls whether we use the DYING state for
## [member life_state]. If disabled, setting the life state to DYING will
## instead set it to DEAD. Disabling this state is useful if the TheaterPiece
## has no actions to perform during the dying state - no animations or anything
## like that.
@export var use_dying_state: bool = true

## This property controls the progression restriction from [member life_state].
## Enabling this will allow us transition backwards, such as from DEAD to
## ALIVE. This is useful for theater pieces that are capable of easily moving
## between life states.
@export var allow_life_regression: bool = false

## Removes the TheaterPiece compliant node from the scene once it enters the
## DEAD state. The TheaterPiece compliant node should do this by calling
## [method Node.queue_free], meaning the node is only removed once it is safe
## to do so (i.e. outside of our programming). This will need to be manually
## implemented in any inheriting interfaces.
@export var remove_on_death: bool = true

#
# Public Variables
#
## This property controls the actor's current life state. This should be set to
## a [enum LifeState] value. To change this actor's life state, you can
## manipulate this property directly.
## [br][br]
## This property's setter will take care of everything for a state transition.
## It will emit [signal alive], [signal dying], and [signal dead] as
## appropriate. It will honor the options given by [member use_dying_state],
## [member allow_life_regression], and [member remove_on_death].
var life_state: LifeState = LifeState.ALIVE:
    set(new_state):
        # Don't update the state if there's no update to do
        if life_state == new_state:
            return
        # If we're not allowing life regression, and the new life state is
        # a regression, then back out
        if not allow_life_regression and new_state < life_state:
            return
        # If this isn't a valid life state, back out
        if not new_state in LifeState.values():
            return

        # Set the life state
        life_state = new_state

        # If we're not using the dying state, but we just set ourselves to the
        # dying state, then skip to DEAD
        if not use_dying_state and life_state == LifeState.DYING:
            life_state = LifeState.DEAD

        # If we're ALIVE, emit a signal
        if life_state == LifeState.ALIVE:
            alive.emit()
        # If we're dying, emit a signal
        if life_state == LifeState.DYING:
            dying.emit()
        # If we're dead, emit a signal (and queue ourselves for removal if
        # relevant)
        elif life_state == LifeState.DEAD:
            dead.emit()

        # If we died, and we're configured to remove ourselves on death, emit
        # the death cleanup signal.
        if life_state == LifeState.DEAD and remove_on_death:
            death_cleanup.emit()

#
# Private Variables
#
# Our different, ongoing status effects; the keys are each ongoing status
# condition's keyname, while the values are the effect nodes themselves.
# TODO: Actually implement this.
var _active_effects: Dictionary = {}


#
# Public functions
#
## Deals the given amount of damage to this theater piece. Sets
## [member life_state] appropriately. Spawns damage floats.
func damage(amount: int) -> void:
    # Catch the current HP
    var start_hp = stats.curr_hp

    # Do the damage
    stats.curr_hp -= amount

    # If we're sending out damage floats, emit the amount of damage done
    if damage_float_text:
        var damage_float = _FLOAT_AWAY_TEXT.instantiate()
        damage_float.text = "-" + str(start_hp - stats.curr_hp)
        float_away_text.emit(damage_float)

    # Emit the damage received
    damage_received.emit(start_hp - stats.curr_hp)

    # If we have any hitpoints, back out
    if stats.curr_hp > 0:
        return

    # If we're not alive, there's no actual status to update
    if life_state != LifeState.ALIVE:
        return

    # If we're using the dying state, move to dying
    if use_dying_state:
        life_state = LifeState.DYING
    # Otherwise, we're dead
    else:
        life_state = LifeState.DEAD


## Heals the theater piece by the given amount. Spawns heal floats.
func heal(amount: int) -> void:
    # If we're not alive, and we're not allowing life regression, then back out.
    if life_state != LifeState.ALIVE and not allow_life_regression:
        return

    # Catch the current HP
    var start_hp = stats.curr_hp

    # Do the healing
    stats.curr_hp += amount

    # If we're sending out healing floats, emit the amount of healing done
    if healing_float_text:
        var healing_float = _FLOAT_AWAY_TEXT.instantiate()
        healing_float.text = "+" + str(stats.curr_hp - start_hp)
        float_away_text.emit(healing_float)

    # Emit the healing received
    healing_received.emit(stats.curr_hp - start_hp)

    # If we don't allow life regression, back out. Nothing for us here.
    if not allow_life_regression:
        return

    # Otherwise, if we're not alive and we have hitpoints, bring ourselves back
    # to life.
    if life_state != LifeState.ALIVE and stats.curr_hp > 0:
        life_state = LifeState.ALIVE

# TODO: Status conditions - we unfortunately can't do this until status
# conditions themselves are overhauled.

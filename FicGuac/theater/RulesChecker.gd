class_name RulesChecker  # Give it a class name for global scope
#
# Description
#
## Script for verifying a script/scene complies with the TheaterPiece rules
##
## We have multiple classes that need similar TheaterPiece functionality, and
## they all have to follow a certain set of rules. To make this easier on
## ourselves, and save code duplication, this script provides static functions
## for verifying a given object is in compliance with these rules


## Get the warnings array for the given
static func get_warnings(target: Object) -> Array:
    var warnings: Array = []
    var warn_msg: String = ""
    var tp: TheaterPiece

    if target.get("theater_piece") == null:
        warn_msg = "Node either lacks the 'theater_piece' field or that field "
        warn_msg += "is set to null!"
        warnings.append(warn_msg)
        return warnings

    # Get the theater piece
    tp = target.theater_piece

    # Check if the theater piece is local to the scene
    if not tp.resource_local_to_scene:
        warn_msg = "Theater Piece is not local to scene! This will cause "
        warn_msg += "problems across all instances of this scene."
        warnings.append(warn_msg)

    # Get the stats
    var stats: CommonStats = tp.stats

    # If it's null, that's bad
    if stats == null:
        warn_msg = "TheaterPiece resource MUST have a Stats resource, or "
        warn_msg += "the program will most likely crash."
        warnings.append(warn_msg)
    # Otherwise, if the stats aren't local to the scene, that's also bad
    elif not stats.resource_local_to_scene:
        warn_msg = "Stats resource of TheaterPiece resource must be local to "
        warn_msg += "scene! Otherwise there will be problems across all "
        warn_msg += "instances of this scene."
        warnings.append(warn_msg)

    return warnings

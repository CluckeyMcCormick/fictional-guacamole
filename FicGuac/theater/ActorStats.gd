class_name ActorStats
extends CommonStats

#
# Description
#
## A resource for common and Actor-specific stats
##
## The Actor Stats resource is the primary stats-and-status resource for actors.
## It builds upon [CommonStats] by adding [Actor]-specific stats.
## [br][br]
## Like the [CommonStats] object, this resource divides the stats into
## "base" stats - what the stats should be initially - and the "current" or
## "effective" status. The idea is that the base stats are fixed, and we can
## always derive calculations from these values. The curr/effective values are
## the values actually used in code, and are directly manipulated/affected by
## status conditions and other outside factors.

#
# Constant Declarations
#
## The minimum allowable move speed. Move speed should never be negative, as
## this could result in odd behavior, like backwards-moving characters.
## Currently, we don't allow characters to stop completely, but they at least
## move INCREDIBLY ULTRA SLOW.
const MIN_MOVE_SPEED: float = 0.001
## The maximum allowable move speed. If an [Actor] moves too fast, this could
## cause engine problems. For right now, we don't really cap the speed.
const MAX_MOVE_SPEED: float = INF

#
# Export Variables
#
## The base move speed in world-units/second. This is the base measure that
## buffs/debuffs are applied to. Although this is limited in the editor, you
## can enter your own value, as the max value is uncapped.
@export_range(0.01, 100, 0.01, "or_greater") var base_move_speed: float = 10.0

#
# Public Variables
#
## The current/effective move speed, in world-units/second. This is the value
## that should be referenced for the purpose of movement.
var eff_move_speed: float = base_move_speed:
    set(new_move_speed):
        eff_move_speed = clamp(new_move_speed, MIN_MOVE_SPEED, MAX_MOVE_SPEED)

# TODO: Signals for variable updates? Could help with updating GUI elements...

@tool  # Must be tool for configuration checking
@icon("res://theater/icons/beech.png")
class_name BaseScenery
extends StaticBody3D

## Base theater piece implementation for StaticBody3D
##
## This class implements the [TheaterPiece] interface on a StaticBody3D.
## That means that it's actually usable, but using this particular class is not
## recommended. You should always use [Scenery] instead, which has the
## complete set of fields we would want for our Scenery. This class only exists
## to isolate the theater piece implementation code from our actual
## Scenery-specific stuff.
## [br][br]
## In a different programming language, we could have used multiple inheritance
## to build out the Scenery classes instead of having to reprogram everything by
## hand. Godot doesn't support that right now, and who's to say it should? It's
## a niche requirement, and those resources are probably being thrown at better
## features.

## Signal emitted when the Scenery enters the ALIVE state. Scenery
## reimplementation of [signal TheaterPiece.alive]. Emits the current Scenery.
signal alive(prop: BaseScenery)
## Signal emitted when the Scenery enters the DYING state. Scenery
## reimplementation of [signal TheaterPiece.dying]. Emits the current Scenery.
signal dying(prop: BaseScenery)
## Signal emitted when the Scenery enters the DEAD state. Scenery
## reimplementation of [signal TheaterPiece.dead]. Emits the current Scenery.
signal dead(prop: BaseScenery)

## Underlying [TheaterPiece] object. This is gives the Prop [TheaterPiece]
## functionanility and allows the Prop to be used as a TheaterPiece object.
## [br][br]
## [b]This CANNOT be null or this Prop will cause the game to crash.[/b]
## [br][br]
## [b]This resource, and all resources under it, MUST be local to the scene or
## you may see odd behavior across all instances of a particular
## [BaseProp].[/b]
## [br][br]
## We use configuration warnings to ensure this is configured correctly, but
## the warning tooltips may lag a bit. In general, clicking directly on the
## warning will give you up-to-date information.
@export var theater_piece: TheaterPiece = null


func _init() -> void:
    # We want our theater piece signals to fire whenever the corresponding
    # signals from the theater_piece object fire. Unfortunately we can't
    # directly bind signals to signals in Godot - but we CAN bind signals to
    # the "emit_signal" function. And then we can do some more trickery off the
    # back of that!

    # Wrap the emit signal function
    var alive_callable = Callable(emit_signal)
    var dying_callable = Callable(emit_signal)
    var dead_callable = Callable(emit_signal)

    # Add the appropriate signal-strings and pass the current Scenery
    alive_callable.bind("alive", self)
    dying_callable.bind("dying", self)
    dead_callable.bind("dead", self)

    # Connect the signals!
    theater_piece.alive.connect(alive_callable)
    theater_piece.dying.connect(dying_callable)
    theater_piece.dead.connect(dead_callable)

    # Connect the theater piece's death cleanup signal to the queue free
    # function, ensuring we clean ourselves up
    theater_piece.death_cleanup.connect(queue_free)

    # Connect the theater piece's float away text signal to our parent's add
    # child signal. This should just add the float away text under the parent
    theater_piece.float_away_text.connect(get_parent().add_child)


# Note: the configuration warning tooltips don't update very quickly, but
# clicking on the warning button next to the node usually works
func _get_configuration_warnings() -> PackedStringArray:
    var warnings: PackedStringArray
    warnings.append_array(RulesChecker.get_warnings(self))
    return warnings


func _process(_delta) -> void:
    if Engine.is_editor_hint():
        update_configuration_warnings()

extends State

## Template scene for implementing tasks
##
## This is the scene from which all other tasks should be derived (inherited).
## It provides a common interface that will make using tasks much simpler.
##
## Tasks are allowed to partially fail or succeed, however the signals only
## allow for this to be expressed as a failure-success binary. For our purposes,
## we only consider 100% whole task completion to be a "success". If we couldn't
## do everything we were assigned to do, that's a failure.
##
## To actually implement a task, add actions to a scene inheriting from this
## one. From there, it's a simple matter of adding code to glue the actions
## together. That code should be in a script that extends this template script,
## though it can be spread to other non-action nodes. ou'll most likely use a
## combination of utility functions and functions responding to signals. Don't
## forget that the initial state of an XSM machine is always the first child
## state in a tree!

# TODO: Make MR/PTR private
# TODO: Redo the initialization system to make task initialization better.

## The Machine Root of this task's parent-machine. This variable offers access
## to the various configurables of the current machine - especially the cores.
var MR

## The Physics Travel Region of this task's parent-machine. This region changes
## very little between different machines and is critical for moving an
## integrating body (and responding to completed movements).
var PTR

## This signal indicates that the task succeeded. If a task succeeds, that means
## it did 100% of what it was supposed to.
signal task_succeeded

## This signal indicates that the task failed. If a task fails, that means it
## did not complete 100% of what it was supposed to. Could be 0%, could be 99%.
## Its therefore the responsibility of whatever manages this task to appraise
## the extent of the failure.
signal task_failed

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Initialize Functions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# We have THREE initialization functions. Yes that's confusing BUT we sometimes
# we need to initialize different components of the task at different times. So
# we have three:


## This function initializes the template's variables. The variables passed here
## are essentially coupling variables - variables that allow us to interface
## with/connect to the host machine. Ergo, this function should be called by the
## host machine!
##
## In particular - the machine root is meant to be the root of the wider
## machine. The physics travel region is the region of the machine detected to
## movement. The target body is the integrating body.
func template_initialize(machine_root, physics_travel_region, target_body):
    # Set our machine root and physics travel region
    self.MR = machine_root
    self.PTR = physics_travel_region

    # Ensure the owner-target corresponds to the target body
    self.target = target_body


## This function initializes those variables that are specific to the current
## task. It frequently requires input from the world - like a list of specific
## items, or a specific target. This can be called from within the host
## machine, but we may want to call it from outside the machine and then
## inject/pass the task into the machine. In order to allow the task to accept
## any possible arguments, it takes in a dictionary argument.
func specific_initialize(arg_dict):
    print("Task Template Specific Initialize called, please overload.")


## This is a convenience function that wraps both the template_initialize
## function and the specific_initialize function.
func initialize(machine_root, physics_travel_region, target_body, arg_dict):
    template_initialize(machine_root, physics_travel_region, target_body)
    specific_initialize(arg_dict)

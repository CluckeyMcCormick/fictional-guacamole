extends "res://motion_ai/common/tasking/core/TaskTemplate.gd"

## Task to flee threats in the immediate area
##
## This task flees all Threat bodies in a machine's Fight-or-Flight area. If
## new Threats enter the Fight-or-Flight area, the task adjusts appropriately.
##
## If the integrating body escapes the *Threat* bodies, it succeeds. If the
## integrating body gets stuck, it fails.

## ArgKey for the specific_initialize function. This arg should be the flee
## distance, in game units. Every time the task flees it moves this distance
## before reassessing the best path (or it at least ATTEMPTS to). If this value
## is too small this may result in the integrating body rapidly stuttering and
## losing ground to whatever it is fleeing. The distance we'll attempt to move
## in one direction before reevaluating. Float or int acceptable.
const AK_FLEE_DISTANCE = "flee_distance"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Task Functions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Initializes the task's specific/unique variables. Requires at least one entry
## with the AK_FLEE_DISTANCE constant.
func specific_initialize(arg_dict):
    # Pass down the flee_distance
    #$FleeAllBody.flee_distance = arg_dict[AK_FLEE_DISTANCE]
    pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Godot Functions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func _on_FleeAllBody_action_failure(failure_code):
    emit_signal("task_failed")


func _on_FleeAllBody_action_success():
    emit_signal("task_succeeded")

extends "res://motion_ai/common/tasking/actions/core/ActionTemplate.gd"

## A task to move to a specific position
##
## This action attempts to move to a specific position. This is a very general
## move action, useful for when you know EXACTLY where you want to move.
##
## This action will fail if the integrating body cannot path or gets stuck too
## many times.

## The position that we're attempting to move to. Should be set (via code) by
## the parent task. Assumed to be a global position Vector3.
var _target_position: Vector3
# How many times have we gotten stuck and repathed as a result?
var _stuck_repath_count = 0

# What's the maximum number of times we'll let ourselves get stuck and perform a
# repath before we just throw in the towel and fail the action?
const MAX_STUCK_REPATH = 3

## Enumeration of possible error values that will be emitted by the
## "action_failure" signal. FC stands for "Failure Code".
enum {
    FC_EXCESS_STUCK,  # We got stuck too many times
    FC_NO_PATH,  # We weren't able to generate a path to the target position
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Extended State Machine Functions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func _on_enter(arg) -> void:
    # Connect the PhysicsTravelRegion functions
    PTR.connect(
        "path_complete", Callable(self, "_on_phys_trav_region_path_complete")
    )
    PTR.connect(
        "error_goal_stuck",
        Callable(self, "_on_phys_trav_region_error_goal_stuck")
    )

    # We just got here - we've been stuck 0 times!
    _stuck_repath_count = 0

    # Path3D to our target entity
    path_to_target()


func _on_exit(arg) -> void:
    # Disconnect the PhysicsTravelRegion functions
    PTR.disconnect(
        "path_complete", Callable(self, "_on_phys_trav_region_path_complete")
    )
    PTR.disconnect(
        "error_goal_stuck",
        Callable(self, "_on_phys_trav_region_error_goal_stuck")
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Signal Processing (and Other) Functions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func path_to_target():
    return
    # Get our LevelInterfaceCore
    var LIC = MR.level_interface_core_node

    # The path we got out of the PathingInterfaceCore
    var path

    # Clear any move data we had, since we're gonna be moving in a new
    # direction
    PTR.clear_target_data()

    # Generate a path to the target position!
    path = LIC.path_between(target.global_transform.origin, _target_position)

    # Now pass the path to our pathing region
    PTR.set_target_path(path, true)

    # If our path is empty...
    if not PTR.has_target_data():
        # Oh. Dang. That's a failure!
        emit_signal("action_failure", FC_NO_PATH)


func _on_phys_trav_region_path_complete(position):
    # We arrived. Hooray! That's a success!
    emit_signal("action_success")


func _on_phys_trav_region_error_goal_stuck(target_position):
    # We're stuck? Huh. Let's increment our counter...
    _stuck_repath_count += 1

    # If we haven't repathed too much, then might as well repath again!
    if _stuck_repath_count <= MAX_STUCK_REPATH:
        path_to_target()
    # Otherwise, we've repathed too much. Throw in the towel - this is a
    # failure!
    else:
        emit_signal("action_failure", FC_EXCESS_STUCK)

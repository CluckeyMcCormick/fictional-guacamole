extends "res://motion_ai/common/tasking/actions/core/ActionTemplate.gd"

## A task for playing an animation
##
## This action plays an animation. This will emit a success once the animation
## has been played. It will only emit a failure if some sort of error breaks the
## action's internal processing.

## Key for the animation to play.
@export var animation_key: String = ""
## If true, the animations are requested with direction - meaning the
## orientation of the parent body could impact which animations get played.
@export var use_direction: bool = true


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Extended State Machine Functions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func _on_enter(arg) -> void:
    # Connect the "demand complete" function so we know when our animation is
    # finished
    MR.connect("demand_complete", Callable(self, "_on_demand_complete"))

    # Now, DEMAND THAT THE ANIMATION BE PLAYED!!!
    MR.emit_signal("demand_animation", animation_key, null, use_direction)


func _on_exit(arg) -> void:
    # Disconnect the "demand complete" function
    MR.disconnect("demand_complete", Callable(self, "_on_demand_complete"))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Signal Processing Functions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func _on_demand_complete(completed_animation_key):
    # If the animation key we were just passed matches ours, then that's a
    # success.
    if completed_animation_key == animation_key:
        emit_signal("action_success")
    # Otherwise, something managed to play ahead of us. Since we cannot confirm
    # what the machine's current state is, we must fail the action.
    else:
        emit_signal("action_failure")

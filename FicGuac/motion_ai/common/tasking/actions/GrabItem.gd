extends "res://motion_ai/common/tasking/actions/core/ActionTemplate.gd"

## Action to grab a single item
##
## This action will grab a single item, adding it to the Item Management Core's
## stack.
##
## This action will fail if it cannot grab the item.
##
## This task has the standard action_success and action_failure signals, as
## defined by the Action template.

## A MasterItem. This is the item we will attempt to grab. Providing a
## non-MasterItem node will most likely crash the game. Should be set (via code)
## by the parent task.
var _target_entity

## F(ailure) C(ode) enumerator - the values of this enumerator will be emitted
## with the action_failure signal. This will help us to clarify exactly how the
## action failed
enum {
    FC_CANT_GRAB_ITEM,  # We can't grab an item, for some reason
    FC_NULL_ENTITY,  # The target entity we're trying to grab was null!
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Extended State Machine Functions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# Grabbing an item is easy! We'll just do that when we enter this state.
func _on_enter(arg) -> void:
    # Get our ItemManagementCore
    var IMC = MR.item_management_core_node

    # Wrap the target in a weak reference
    var target_wrap = weakref(_target_entity)

    # Clear any move data we had.
    PTR.clear_target_data()

    # If we don't have an item, we don't have anything to do. That's a failure!
    if target_wrap.get_ref() == null:
        emit_signal("action_failure", FC_NULL_ENTITY)
    # If we can't grab it for whatever reason, that's a failure!
    elif not IMC.can_grab_specific_item(_target_entity):
        emit_signal("action_failure", FC_CANT_GRAB_ITEM)
    # If we do have an item, then drop it. That's a success!
    else:
        IMC.grab_item(_target_entity)
        emit_signal("action_success")

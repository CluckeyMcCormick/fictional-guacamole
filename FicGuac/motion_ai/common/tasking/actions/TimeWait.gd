extends "res://motion_ai/common/tasking/actions/core/ActionTemplate.gd"

## A task for doing nothing - just waiting.
##
## This just waits for a specified amount of time. This is good if you want to
## implement a cooldown, or some sort of freeze of some kind.

# What's the string name associated with the timer we instantiate?
const WAIT_TIMER_NAME = "WAIT_TIMER"

# The MR and PTR variables are declared in the ActionTemplate scene that exists
# above/is inherited by this scene.

## The desired time to wait, in seconds.
@export var wait_time: float


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Extended State Machine Functions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Start the timer as soon as we enter
func _on_enter(arg) -> void:
    add_timer(WAIT_TIMER_NAME, wait_time)


# Once the wait is done, the action suceeds!
func _on_timeout(name) -> void:
    # If the timer ends, then items time to start falling!
    match name:
        WAIT_TIMER_NAME:
            emit_signal("action_success")

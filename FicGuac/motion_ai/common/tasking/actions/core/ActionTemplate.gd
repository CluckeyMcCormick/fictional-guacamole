extends State

## Template scene for implementing an action
##
## This is the scene from which all other actions should be derived (inherited).
## It provides a common interface that will make using actions much simpler.
##
## Unlike tasks, actions need clearly delineated success or failure. The action
## must fail or succeed. If you've designed an action that can somehow partially
## succeed, then that's not an action - it's a simplified task. Actions are
## really the most discrete, atomic possible breakdown of doing something.
##
## It is not recommended to allow actions to have children states.
##
## To actually implement an action, you need to implement at least one of XSM's
## "abstract public functions" - see addons/xsm for more on that.
##
## One final note: when correctly integrated into a task, the "target" field
## will be the parent-machine's integrating body. This variable is inherited
## from XSM's State class.

## The Machine Root of this task's parent-machine. This variable offers access
## to the various configurables of the current machine - especially the cores.
## Note that this resolves on ready by directly accessing the parent's MR
## variable, since it is assumed that the action's parent will be a Task node.
## Ergo, all actions must be immediate children of a Task in order to function
## and not crash the game.
@onready var MR = get_parent().MR

## The Physics Travel Region of this task's parent-machine. This region is
## critical for moving an integrating body (and responding to completed
## movements). Note that this resolves on ready by directly accessing the
## parent's MR variable, since it is assumed that the action's parent will be a
## Task node. Ergo, all actions must be immediate children of a Task in order to
## function and not crash the game.
@onready var PTR = get_parent().PTR

## This signal indicates that the action succeeded.
signal action_success

## This signal indicates that the action failed. It is up to the task to decide
## whether this constitutes a failure or not.
signal action_failure(failure_code)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Utility Functions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## This resets an action by calling the action's XSM exit and enter functions.
## In this way, it effectively resets the function. This function is very useful
## for when the current action in a task needs to be repeated after success
## - maybe the variables need to updated, or we're trying something *X* number
## of times as error handling.
##
## This should work perfectly so long as the action has no child states. That's
## a very fundamental assumption that should not be broken. If your action needs
## to have a child state to work, then it must be something incredibly
## complicated and would likely be better as a series of separate actions.
##
## Now, since there a four XSM transition functions that must be called in order
## to reset an XSM state, there are four arguments to this function:
##
## - before_arg: Argument supplied to the _before_exit function.
## - exit_arg: Argument supplied to the _on_exit function.
## - enter_arg: Argument supplied to the _on_enter function.
## - after_arg: Argument supplied to the _after_enter function.
##
## All of these arguments are optional; they default to null if not provided.
func simulated_reset(
    before_arg = null, exit_arg = null, enter_arg = null, after_arg = null
):
    # TODO: This function has always been a bit dangerous since we mess with
    # XSM's underlying functionality directly. The current version of XSM allows
    # you to change the state to the current state - could that replace this?

    # If this action is not active, don't do the reset. That could mess things
    # up.
    if not self.active:
        return
    # Simulate an exit
    self._before_exit(before_arg)
    self._on_exit(exit_arg)
    # Simulate an enter
    self._on_enter(enter_arg)
    self._after_enter(after_arg)

# Actions
This directory holds our various actions. Each action is a single *XSM* `State`
and provides a common interface.

These actions exist so that we don't need to constantly duplicate code for
common actions. This will help keep things consistent between tasks, which will
further keep things consistent between different machines and AI constructs.

A couple of quick notes about the current design philosophy of actions:

1. *Actions should be very specialized. They should do one thing and one thing
only.*
	- For example, an action may be dropping an item, or grabbing an item, or
	moving to a very particular position. Another way to look at it is that
	states must be *atomic*, as actions are the lowest level breakdown of what
	an AI can do.
	
2. *Actions should generally accept a singular input.*
	- Whereas tasks should be designed to do multiple things, actions - being
	atomic - should only accept singular inputs. It is the responsibility of the
	task to chain actions together as needed.

3. *Actions should never repeat. They must succeed or they must fail.*
	- Again, this relates to the atomic nature of actions. It is up to the tasks
	that are *composed* of actions to decide what actions bear repeating.

4. *Actions must hard fail or hard succeed. No partial conclusions.*
	- Because actions are atomic, they must either fail or succeed. There can be
	no partial conditions. Anything that could "partially" fail or succeed is a
	bad candidate for an action, and could probably be decomposed into several
	separate actions.

Actions may or may not be error-tolerant. Your decision.

### Core
This directory stores the Core scenes that make up the *Action*'s core
interface.  See this directory for further explanation of the interface quirks
of Actions. 


# Common (Motion AI)
This directory holds all the common AI scenes and assets. There are two types of
AI scenes: **Cores** and **Machines**. The *Cores* serve as the configuration
interface and information store, while the *Machines* do the actual processing.
By their nature, the *Cores* are reusable and generalized. *Machines* are much
more specialized.

The *Cores* exist separate from the *Machines* in order to give a common
configuration interface and functionality across multiple machines. Also,
placing all of the appropriate configuration items on the machine level would
lead to a MASSIVE configuration menu. I'd like to try and avoid that.

The *Machines* and *Cores* are designed to be as plug-and-play as possible - for
example, the required *Cores* are presented as `Node` configurables for each
machine. However it's impossible to completely eliminate code integration (in
some instances). So, sometimes, there's a lot more plugging than playing. We'll
capture the specifics, oddities, and configuration items in this documentation.

Where some amount of coding is required as part of the plugging, we typically
refer to this action as "coupling"; functions created as part of coupling are
referred to as "coupling functions". This is especially true for signals.

### Tasking
A key component of our main series of AI is the use of tasks - little machines
that can be inserted into certain larger machines and perform moment-to-moment
decisions. Each task has a common interface but implementation is largely up to
the individual tasks themselves.

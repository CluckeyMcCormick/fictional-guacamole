@tool
extends Node

## A core for providing machines with information about the level/world.
##
## The LevelInterfaceCore is the core that allows our AI to navigate and
## interact with the map independent of any other externalities. It provides
## configurables for an easy interface, as well as several utility functions to
## allow any machine to path or check it's position.
##
## Unfortunately, because this node requires access to a Navigation node of some
## description, and those almost invariably exist outside of any integrating
## bodies, there will likely be some integration code required.

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Variable Declarations
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## The node we'll use to navigate the world - can either be a vanilla Godot
## Navigation node or a DynamicNavigationMesh node from Milos Lukic's Godot
## Navigation Light.
##
## If you're having trouble decoding what node that's really supposed to be,
## just remember that it's the node that actually has the pathing functions
## attached to it.
##
## This configurable is a NodePath - since any navigation resource will
## typically exist outside of an integrating body, you will likely need to do a
## hot/runtime configuration, most likely in some parent node's _ready function.
## I recommend getting the full NodePath by calling the get_path function on the
## appropriate node.
@export var navigation_node: NodePath:
    set = set_navigation_node
# We resolve the node path into this variable.
var nav_node

## The node we'll use to drop items in the world. When our machine drops items,
## we need a parent node to place the items under. What should that node be?
##
## This configurable is a NodePath - since any navigation resource will
## typically exist outside of an integrating body, you will likely need to do a
## hot/runtime configuration, most likely in some parent node's _ready function.
## I recommend getting the full NodePath by calling the get_path function on the
## appropriate node.
@export var item_parent_node: NodePath:
    set = set_item_parent_node
# We resolve the node path into this variable.
var item_node

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Setters and Getters
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# Set the navigation node. Unlike most cores, we actually resolve the node path
# whenever it gets set. Also updates our configuration warning.
func set_navigation_node(new_navigation_node):
    navigation_node = new_navigation_node

    # If we're in the engine, update our configuration warning
    if Engine.is_editor_hint():
        update_configuration_warnings()
    # Otherwise, update the nav node
    else:
        nav_node = get_node(navigation_node)


# Set the item parent node. Unlike most cores, we actually resolve the node path
# whenever it gets set. Also updates our configuration warning.
func set_item_parent_node(new_item_parent_node):
    item_parent_node = new_item_parent_node

    # If we're in the engine, update our configuration warning
    if Engine.is_editor_hint():
        update_configuration_warnings()
    # Otherwise, update the drop node
    else:
        item_node = get_node(item_parent_node)


# This function is very ugly, but it serves a very specific purpose: it allows
# us to generate warnings in the editor in case the KinematicDriver is
# misconfigured.
func _get_configuration_warnings():
    # (W)a(RN)ing (STR)ing
    var wrnstr = ""

    # Get the navigation node - but only if we have a node to get!
    if not navigation_node.is_empty():
        nav_node = get_node(navigation_node)

    # Test 1: Check if we have a navigation node
    if nav_node == null:
        wrnstr += "No Navigation Node specified, or path is invalid!\n"
        wrnstr += "Level Interface Core does allow for runtime configuration\n"
        wrnstr += "via set_navigation_node if preconfiguring is impractical.\n"

    # Get the item drop parent node - but only if we have a node to get!
    if not item_parent_node.is_empty():
        item_node = get_node(item_parent_node)

    # Test 2: Check if we have a item parent node
    if item_node == null:
        wrnstr += "No Item Parent Node specified, or path is invalid!\n"
        wrnstr += "Level Interface Core does allow for runtime configuration\n"
        wrnstr += "via set_item_parent_node if preconfiguring is impractical.\n"

    return wrnstr


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Godot Processing - _ready, _process, etc.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func _ready():
    # If we have a navigation node...
    if navigation_node:
        # Get it! This may not have been resolved yet, since this node may have
        # not been in the scene tree before now
        nav_node = get_node(navigation_node)

    # If we have an item parent node...
    if item_parent_node:
        # Get it! This may not have been resolved yet, since this node may have
        # not been in the scene tree before now
        item_node = get_node(item_parent_node)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Core functions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Returns the position of the given spatial node on the core's Navigation Node
## configurable - i.e. it's "algorithmic" position. This is most often used for
## goal checking - i.e. seeing where we are from a world-mesh perspective.
func get_adjusted_position(in_body: Node3D):
    pass


#   # Get our current position
#   var curr_pos = in_body.global_transform.origin
#   # What's our adjusted vector?
#   var adjusted = Vector3.ZERO
#
#   # If we're using a navigation node, we can just use get_closest_point. Easy!
#   if nav_node is Navigation:
#       adjusted = nav_node.get_closest_point(curr_pos)
#
#   # Otherwise, if it's a spatial, we don't have any recourse but to ASSUME
#   # that we're dealing with a DetourNavigationMesh.
#   elif nav_node is Node3D:
#       # This mesh doesn't actually give us a method to easily access where we
#       # are on the mesh. So, we'll cheat. We'll just path FROM our current
#       # position TO our current position. I don't know the performance
#       # ramifications of this, but HOPEFULLY its small. Or some update saves
#       # us from this madness...
#       adjusted = nav_node.find_path(curr_pos, curr_pos)
#       # Unpack what we got back, since we want the first value of a
#       # PoolVector3 array stored in a dict.
#       adjusted = Array(adjusted["points"])[0]
#
#   # Otherwise... Guess I have no idea what's happening. Let's just use the
#   # current position.
#   else:
#       adjusted = curr_pos
#       push_warning("LIC couldn't get adjusted position due to invalid/null nav_node.")
#
#   return adjusted


## Returns an Array-path of points that start at from_point and go to
## to_point. May return an empty path.
func path_between(from: Vector3, to: Vector3):
    pass
#   var path
#
#   # If we're using a navigation node, we can just use get_simple_path. Easy!
#   if nav_node is Navigation:
#       # Get the path
#       path = Array(nav_node.get_simple_path(from, to))
#
#   # Otherwise, if it's a spatial, we don't have any recourse but to ASSUME
#   # that we're dealing with a DetourNavigationMesh.
#   elif nav_node is Node3D:
#       # Get the path
#       path = nav_node.find_path(from, to)
#       path = Array(path["points"])
#
#   # Otherwise... Guess I have no idea what's happening. Let's just not path.
#   else:
#       path = []
#       push_warning("LIC couldn't generate path because of invalid/null nav_node.")
#
#   # If we have more than one node along the path...
#   if len(path) > 1:
#       # Then remove the first one, since this seems to always be our start -
#       # in other words, where we already were. If we left that in, it would
#       # mean at least one cycle of no movement. Might not seem that bad, but
#       # it could stack up VERY quickly.
#       path.pop_front()
#
#   return path

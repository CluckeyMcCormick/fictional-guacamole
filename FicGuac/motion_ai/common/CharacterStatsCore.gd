extends "res://status/CommonStatsCore.gd"

## Core to contain AI status variables
##
## The [i]CharacterStatsCore[/i] is the core [i]stats and status[/i] component
## of our AI system. It is the singular reference point for stats (i.e. movement
## speed) and status (i.e. health points).
##
## This core divides the stats into "base" stats - what the stats should be
## initially - and the "current" or "effective" stats.
##
## This is a AI-specific version of the [code]CommonStatsCore[/code], found in
## the godot project's *status* directory. It inherits configurables, variables
## and functions from that scene - hence, it's recommended that you look at that
## scene for further reference.

## The base move speed in world-units/second. This is the base measure that
## buffs/debuffs are applied to.
@export var base_move_speed: float = 10

## The current/effective move speed, in world-units/second. This is 'effective'
## in the sense that it is the move speed after buffs and debuffs have been
## applied. This is the value that should be referenced for the purpose of
## movement.
var eff_move_speed = base_move_speed

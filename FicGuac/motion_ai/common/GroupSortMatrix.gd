class_name GroupSortMatrix
extends Resource

## A resource that assigns different group strings to different categories.
##
## We identify different spatial elements in this game - hazards, items,
## projectiles, AI, etc - using Godot's [code]Node[/code] [i]Groups[/i]. They're
## the closest things Godot has to some sort meta-tagging system. In order to
## build a robust AI system, we can't just use global tags (that'd be a bit
## painful managing global strings like that, anyhow). No, instead we need a way
## to group tags into universal categories, and leave it up to an integrating
## body to decide which groups fall into which category.
##
## This custom (scripted) resource provides an interface for listing which groups
## fall under which category.

## A [code]PoolStringArray[/code]. Each string should be a group that an
## integrating body treats as a [i]Threat[/i]. Threats are different from
## enemies, representing a danger that can't be addressed and should instead be
## fled from.
@export var threat_groups: PackedStringArray

## A [code]PoolStringArray[/code]. Each string should be a group that an
## integrating body treats as a [i]Goal[/i]. Goals are just a temporary grouping
## for anything and everything good. We'll expand it into multiple categories
## later.
@export var goal_groups: PackedStringArray

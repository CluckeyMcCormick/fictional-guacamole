### State Composition
The Machine is composed of three separate XSM machines that feed in to one
another. The first is the *Physics Travel Region*, which captures the earlier
*Kinematic Driver Machine* in miniature. It has the following nodes:

1. *Falling*
1. *OnGround*
1. *Walk*
1. *Idle*

The *OnGround* state is a super state containing the *Walk* and *Idle* states.

The second machine is whatever task the machine is currently performing. This is
handled by a *TaskManager* node, which we'll not cover here. The task's root
node will actually be an XSM `StateRoot`. The machine may or may not have a task
at any given time, meaning the number of machines constantly fluctuates between
two and three.

The final region is the *GoalRegion*, which assigns different tasks. It is, in
effect, the actual "thinking" ethos component. It has the following nodes:

1. *Idle*
1. *Generic Tasked*
1. *Flee*

The Tree Structure can be observed below. Note that these are separate because
they are no longer part of a contiguous XSM:

![Image](./doc_images/TCM.hierarchy.png "TCM Hierarchy Tree")

### State Control Flow

As we have two regions, we have effectively have two sub-machines:

##### Goal Region
Our default state is the *Idle* state. If, at any point, we detect a *Threat*
body intruding in our *Fight or Flight* sensory area, we transition to the
*Flee* state. We return from the *Flee* state to the *Idle* state once we are
safe.

In the *Idle* state, we wait for the amount of time specified by the *Idle Wait
Time* configurable before issuing ourselves a *Wander* task. Once that task
completes, we again wait for the configured time before assigning another
*Wander* task - so on and so forth. If a task gets ordered, we move to the
*Generic Tasked* state.

In the *Generic Tasked* state, we perform the assigned task. If we complete the
task - success or failure - we move back to the *Idle* state. If we encounter a
threat, we drop the task and move to the *Flee* state.

The *Goal Region* process can be observed in this image:

![Image](./doc_images/TCM.flow.state.GR.png "TCM State Flow Control Tree, Goal
Region")

##### Physics Travel Region
The machine defaults to the *Idle* state. If we have any *move/pathing data*, we
will move towards it via the *Walk* state. Once we reach a target position, the
next target position will be taken from our path data. Once we have no *move
data*, we return to idle.

Meanwhile, the *OnGround* state is constantly probing downwards to ensure we are
on the ground. If we aren't, we begin falling, but *without* interrupting the
subordinate states. This allows us some "airtime" where we walk on air. The
airtime corresponds to the `KinematicCore`'s *Fall State Time Delay*
configurable. Once we've fallen for that time duration, we transfer to the
*Falling* state proper.

While in the *Falling* state, the only thing we do is fall. That's it. Once we
hit the ground, we go to *Idle*.

The *Goal Region* process can be observed in this image:

![Image](./doc_images/TCM.flow.state.PTR.png "TCM State Flow Control Tree,
Physics Travel Region")

Note that the *OnGround* state contains the *Idle* and *Walk* states.

### State Data Flow

The manipulation and flow of data in tightly controlled to try and keep things
clean. Keep that in mind when looking at this terribly messy graph - IT COULD
ALWAYS BE WORSE!

Here "data flow" refers to either observing and reacting to changes in data
(i.e. signals), **or** directly manipulating variables.

The *TCM* data flow is represented here, where blue is data manipulation and red
is a signal reaction. Data flow from the *Sensory Sort Core* is dashed to help
make the graph more legible:

![Image](./doc_images/TCM.flow.data.png "TCM Data Flow Tree")

Observe the path of data manipulations. Save for the state-read variables -
`goal_key`, `physics_travel_key`, `_curr_orient` - the data manipulation flows
left. The highest level of the tripartite architecture, the *Goal Region*,
manipulates the *Task Manager Region*'s current task. The *Task Manager Region*,
being the middle level, manipulates the *Physics Travel Region*. The
*Physics Travel Region*, being the lowest level, doesn't manipulate either of
the other regions.

What flows "up" through the levels are the signals - the *Physics Travel Region*
forces decisions in the current task-action via signals, and the *Task Manager
Region* forces decisions in the current goal-state via signals.

The *Sensory Sort Core*, which acts as the machine's sensory apparatus, emits
signals that are caught by both the *Task Manager Region* and the *Goal Region*.
While not spectacular it is safe because the *Core* is never directly
manipulated. In addition, because the *Task Manager Region* is subordinate to
the *Goal Region*, decisions in the more important *Goal Region* will override
decisions in the *Task Manager Region*. In other words, it doesn't matter if
both regions get the same signal; the **most important signal** will always win
out.

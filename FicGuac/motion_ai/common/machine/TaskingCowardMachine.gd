@tool
extends Node

## The principle AI scene for this project - the brain capable of driving bodies
##
## This machine is a common AI used by all of our actors. It uses a tripartite
## architecture. The three levels correspond to what we would consider
## high-level, mid-level, and low-level processing. These different levels are
## implemented as wholly separate machines instead of regions within the same
## machine. Since this is externally indistinguishable, we still refer to this
## as a machine for documentation purposes - though it is not a true XSM
## machine.
##
## At the highest level, we have the Goal Region, which evaluates the current
## situations and issues tasks. Think of this as the AI's reasoning. Each state
## of the Goal Region can be seen as a different overarching goal for the AI to
## achieve.
##
## At the middle level, we have the Task Manager. The task manager handles the
## different tasks assigned by the Goal Region. These tasks are machines
## themselves and give us a generic interface for making any and all AI perform
## actions.
##
## At the lowest level, we have the Physics Travel Region. This region handles
## movement and collision, allowing the higher levels to simply deal with
## whether or not the task was successful (instead of intense semantics such as
## resolving collisions against collisions or testing for being stuck).
##
## This machine operates in three modes - in Idle mode, the machine
## occasionally wanders in a random direction. In Flee mode, the machine moves
## away from whatever the perceived Threats are. When so ordered (via a
## function), the machine will move into the Generic Tasked state, performing a
## specific task. In that way, this machine acts as our testbed for tasks in
## general. Note that the machine will enter Flee mode from either Idle or the
## Move Tasked state.

## The node that is integrating this machine. This is required to be a
## KinematicBody node. This will be the body moved by the Physics Travel Region,
## and will be the target operated on by any given tasks.
@export var integrating_body: NodePath
# We resolve the node path into this variable.
var integrating_body_node

## The KinematicCore for this machine to use. We require such a node in order to
## actually move the machine.
@export var kinematic_core: NodePath
# We resolve the node path into this variable.
var kinematic_core_node

## The SensorySortCore for this machine to use. We need a Sensory Sort Core so
## that we can react to the world around us (i.e. physics bodies).
@export var sensory_sort_core: NodePath
# We resolve the node path into this variable.
var sensory_sort_core_node

## The LevelInterfaceCore for this machine to use. We need a Level Interface
## Core so we can interface with different elements of the current level
@export var level_interface_core: NodePath
# We resolve the node path into this variable.
var level_interface_core_node

## The ItemManagementCore for this machine to use. We require this in order to
## interact items - specifically picking up and dropping items.
@export var item_management_core: NodePath
# We resolve the node path into this variable.
var item_management_core_node

## The CharacterStatsCore for this machine. We require this to keep track of the
## machine's current status - including health and current speed.
@export var character_stats_core: NodePath
# We resolve the node path into this variable.
var character_stats_core_node

## When in the Idle state, the machine waits for a certain amount of seconds
## before transitioning to the Wander state. This configurable controls the
## duration of that time period.
@export var idle_wait_time = 1.0  # (float, 0.0, 6.0)

## When the machine is in the wander state, it will randomly pick a direction
## and move in that direction. How far does it move in that direction?
@export var wander_distance = 5  # (int, 1, 10)

## When the machine is in the flee state, it will calculate the best direction
## to move given what it is fleeing from. How far does it move in that
## direction? Note that it has been observed that too small a distance will
## cause the integrating body to do a quasi-stutter move. The sum effect is a
## very SLOW fleeing process.
@export var flee_distance = 1.0  # (float, .1, 10)

# Does this machine wander around when idle?
@export var idle_wander: bool = true
# Does this machine flee in the face of danger?
@export var flee_behavior: bool = true

## A Vector3, where each axis is the current (rough) heading on each axis. The
## number is actually equivalent to the last updated velocity on each axis -
## however, it should only really be used to gauge "heading" or "orientation".
##
## The values are irregularly updated in order to preserve continuity between
## states. This is particularly necessary for our sprites, which require an
## angle calculated from this Vector3. If this were to reset to (0, 0, 0) when
## not moving, the sprites would jerk into a common direction when at rest.
## Always.
var _curr_orient = Vector3.ZERO

## A string, indicating the current Physics-Travel state (i.e. Idle, Walk,
## Falling, etc.). Useful for setting the sprites animation as well as
## debugging/display purposes.
var physics_travel_key = ""

## A string, indicating the current Goal state (i.e. Idle, Wander, Flee, etc.).
## Mostly used for debugging/display purposes, but can also be used to flavor the
## sprite's animation. For example, we may use an alternate Flee animation for
## when we're in the Walk Physics-Travel state if we're in the Flee Goal
## state.
var goal_key = ""

# The "movement" hint that is also used for determining our current animation -
# i.e. "moving", "falling", "charging", etc. This hint is typically used to
# choose the right kind of passive/repeating animation.
var movement_hint = "idle"
# The "attitude" hint that is used for determining our current animation - i.e.
# is the machine "angry", "neutral", "scared", etc.? The attitude is typically
# used to flavor an animation instead of driving it.
var attitude_hint = "neutral"
# The demand animation key, used when we specifically demand that an animation
# be played.
var demand_key = ""

# The projected movement is how far we THINK we'll go in a strictly defined
# period of time. This helps us prefrome move-to-intercept actions on any
# implementing body much easier.
var _projected_movement = Vector3.ZERO

# Okay, so the machine can get stuck in a chicken-and-egg situation with regards
# to configuration. The children states require access to several
# variables/nodes that SHOULD be setup in the _ready function of this script.
# However, our underlying state_root script calls the appropriate _on_enter
# functions for our default nodes. These then reference the aformentioned
# variables, RUINING EVERYTHING. To get around this, our configuration is
# performed in a different function, and whether we have configured or not is
# tracked by this variable.
var _machine_configured = false

## This signal indicates the machine was given a specific task to perform.
## Should only really be used (received) by one of the region sub-machines, but
## can be used for diagnostic purposes.
signal task_assigned(task)

## This signal fires when a task fails or succeeds.
signal task_complete_echo

# This signal fires when the machine needs a specific animation to be played.
signal demand_animation(animation_key, target, use_direction)
# This signal fires when the machine's specific animation is finished.
signal demand_complete(animation_key)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Godot Processing - _ready, _process, etc.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# We can make this _ready function without worrying about the underlying state
# code. That will get called after this function.
func _ready():
    # Back out if we're in the engine - this scene has to be a tool, so we're
    # gonna need to sidestep some dumb errors.
    if Engine.is_editor_hint():
        return

    # If the machine isn't configured, then configure it!
    if not _machine_configured:
        _force_configure()


func _force_configure():
    # Get all the nodes
    integrating_body_node = get_node(integrating_body)
    kinematic_core_node = get_node(kinematic_core)
    sensory_sort_core_node = get_node(sensory_sort_core)
    level_interface_core_node = get_node(level_interface_core)
    item_management_core_node = get_node(item_management_core)
    character_stats_core_node = get_node(character_stats_core)

    # The target has to be a CharacterBody3D
    assert(typeof(integrating_body_node) == typeof(CharacterBody3D))  #,"Integrating Body must be a CharacterBody3D node!")
    # Also, we need a KinematicCore
    assert(kinematic_core_node != null)  #,"A KinematicCore node is required!")
    # AAAAAND a we need a SensorySortCore
    assert(sensory_sort_core_node != null)  #,"A SensorySortCore node is required!")
    # Can't forget the Level Interface Core!
    assert(level_interface_core_node != null)  #,"A LevelInterfaceCore node is required!")
    # The Item Management Core ALSO needs to be here
    assert(item_management_core_node != null)  #,"An ItemManagementCore node is required!")
    # We would be ABSOLUTELY NOWHERE without the Character Stats Core!
    assert(character_stats_core_node != null)  #,"A CharacterStatsCore node is required!")

    # Machine is configured!
    _machine_configured = true


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Utility Functions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## Gives the machine a specific task to perform. The task should already be
## initialized using the specific_initialize function. The template_initialize
## function will be called by the machine.
func give_task(task):
    emit_signal("task_assigned", task)


func complete_demand(animation_key):
    emit_signal("demand_complete", animation_key)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Signal Functions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
func _on_TaskManagerRegion_current_task_failed(task):
    emit_signal("task_complete_echo")


func _on_TaskManagerRegion_current_task_succeeded(task):
    emit_signal("task_complete_echo")

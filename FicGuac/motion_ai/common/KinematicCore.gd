extends Node

## A core to capture key kinematic variables for movement.
##
## The KinematicCore is the core motion component of our AI. It governs
## the movement characteristics of an AI. This is mostly basic movement related
## stuff - how fast we fall, how far we must fall for it to be a true fall, a
## permanent float height, delayed fall time, etc.
##
## There was an observed issue with the older KinematicDriver node where it was
## consistently undershooting and overshooting it's target position. This
## resulted in an infinite loop of the driver's body rapidly vibrating back and
## forth in microscopic steps. We called this the Microposition Loop Error.
##
## To track and detect the "Microposition Loop" Error, we keep a "history" of
## our distance-to-target. When we detect this error, we handle it by slowly
## increasing the goal tolerance. We do so in a series of steps, until we either
## reach our goal or decide we're well and truly stuck.
##
## Exactly why and where this happens is harder to pin down. I've noticed that
## it tends to happen at the intersection of edges - the edge of a navigation
## mesh that lies along a y-height difference-edge. I suspect it's got something
## to do with the interaction between the driver body's collision model, the
## KinematicCor's floating stuff, and where the navigation mesh seems to think
## we are. It doesn't seem to happen as often with drive bodies that have smooth
## collision models (i.e. NOT SQUARES) so I recommend using something like a
## capsule. It also seems to have something to do with a body that moves too
## quickly.

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Variable Declarations
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## When determining whether a KinematicBody is on the ground or not, we do a
## fake move downwards. Even if the body is 100%, undeniably on the ground, this
## fake move usually returns a non-zero fall length. Ergo, we need an absolute
## minimum fall distance before we actually fall. This value is the absolute
## minimum distance a KinematicBody has to fall in the floor test to be
## considered "falling" or "not on the floor".
const MINIMUM_FALL_HEIGHT = .002

## In order to detect certain errors (i.e. Microposition Loop, Stuck, etc.), we
## measure distance-to-target and store it in an array. How big is that array -
## i.e. how big is our history?
const TARG_DIST_HISTORY_SIZE = 24  # 24 is enough to capture most of our errors

## Detecting certain errors relies on any entries recurring at least a specified
## number of times. How many duplicate entries do we need to detect for us to
## send out a stuck signal? Keep in mind that is tested AFTER the value is added
## to the array. Ergo, the check will always return at least 1, and the value
## should be greater.
const TARG_DIST_ERROR_THRESHOLD = 3

## Once we've detected certain errors, we slowly increment the goal tolerance
## using a set step value (see the "Tolerance Error Step" configurable).
## However, we only do that as many times specified by this constant before we
## just assume that we're stuck. How exactly that's handled depends on the
## implementation of the machine. This value is inclusive, i.e. error will be
## emitted when tolerance iterations exceed this value.
const MAX_TOLERANCE_ITERATIONS = 5

## Because movement in Godot has a precision of ~6 decimal places, our error
## detection could be hit or miss if we were looking for EXACT matches. Instead,
## we'll round the history entries (and our checks) to this decimal position,
## using the stepify() function. This simplifies error checking and makes it
## more robust.
const ERROR_DETECTION_PRECISION = .01

## The fallback move speed is how quick the KinematicBody moves, in
## units-per-second. This is a fallback since more complex move speed (i.e.
## influenced by status effects) is supposed to be provided by the
## CharacterStatsCore.
@export var fallback_move_speed: float = 10

## The fall speed is how quick the KinematicBody falls when not on the floor, in
## units-per-second. 9.8 units/second, which is the sort of bog-standard for
## gravity, is the recommended fall speed.
@export var fall_speed: float = 9.8

## There might be instances where we don't want to stop exactly on the goal
## position. This configurable allows the body to stop when within a certain
## distance of the target, rather than being precisely at the target.
@export var goal_tolerance: float = 0.1

## The tolerance error step is used whenever we try to correct a microposition
## error. Every time we need to increment the effective goal tolerance, we go up
## by this amount.
@export var tolerance_error_step: float = .05

## This configurable allows us to float the KinematicBody a fixed distance above
## the ground. While it does make the KinematicBody float, this was actually
## intended as a mechanism for stepping-up ledges. The KinematicBody behaves
## differently depending on the current collision shape, but I was finding that
## running into a small step was mostly stopping the body entirely.
##
## By shrinking the collision shape upward, and adjusting the float height
## appropriately, the body takes on an effective height but will also step
## upward whenever it comes to a small ledge or step.
##
## The value is equivalent to the maximum height it is possible for the
## KinematicBody to step up.
@export var float_height: float = 0

## Once we enter a fall state, we're hard locked into it, and whatever machine
## we've integrated with will stop moving horizontally. However, we don't always
## want to transition to a fall state. Could you imagine if you locked up or did
## a ragdoll flop every time you stepped off a curb? No, that's no good.
##
## So, we have a time delay. This many seconds of uninterrupted falling and
## we'll transition to the fall state. Think of it as a sort-of coyote time.
## This will help make things smoother for us.
@export var fall_state_delay_time: float = .1

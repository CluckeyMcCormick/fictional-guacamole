extends CharacterBody3D

## The base for the main NPC of the game.
##
## The basic pawn. Contains a basic script, some sprites, and what we
## collectively refer to as the *Kinematic profile*.
##
## The *Kinematic profile* consists of the `KinematicCore` and the collision
## shapes that help define how the Pawn moves. We define this in the base class
## so that all the derived scenes have the same profile. This helps keep things
## consistent, but also allows us to test different aspects of the *Kinematic
## profile* separately. That's really important, given the complex interactions
## between elements of the *Kinematic profile*.
##
## You'll notice that we have three collision shapes. The "actual" collision
## shape is used in conjunction with the `KinematicCores`'s *Float Height*
## configurable to allow the Pawn to step up ledges. The other two are guide
## shapes; we mostly use them to help measure the Pawn's ideal and effective
## sizes.
##
## Currently, the pawn's actual collision shape is a capsule. This was done so
## that the Pawn could easily move over and around objects without getting
## hooked on things (a common problem for boxier shapes). The Pawn's effective
## step-up height is ~0.5 units - exactness is difficult because of interplay
## between the *Float Height* and the capsule's rounded bottom.

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Constant Declarations
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# When we die, the pawn turns into a corpse. To ensure the corpse falls over, we
# apply a spin in a random direction. What are the minimum and maximum angular
# velocity, in radians/sec? Note that this is applied separately on each axis,
# and will either be positive or negative.
const DEATH_SPIN_MINIMUM = PI / 6
const DEATH_SPIN_MAXIMUM = 2 * PI

# We'll preload all the weapon frames we need since we'll have to change them on
# the fly.
const short_sword_frames = preload(
    "res://motion_ai/pawn/weapon_sprites/pawn_short_sword_frames.tres"
)

# We need the scene for the pawn's corpse so we can spawn it when we die.
const corpse_scene = preload("res://items/corpses/PawnCorpse.tscn")

## Although this enumeration is unnamed, we generally refer to this enumeration
## as the [i]Cardinal[/i](s). Used for easy delineation of directions - very
## important for our 8-directional sprites.
enum {
    # The Primary Cardinals
    EAST = 0,
    NORTH = 2,
    WEST = 4,
    SOUTH = 6,
    # The Intercardinals
    NOR_EAST = 1,
    NOR_WEST = 3,
    SOU_WEST = 5,
    SOU_EAST = 7,
}

## Enumerates the different pieces of equipment the Pawn can have equipped -
## these [code]Enum[/code] values correspond to the [i]Equipment[/i]
## configurable. Allows for easy configuration of the equipped weapon.
enum Equipment {
    NONE,  ## No Weapon
    SHORT_SWORD,  ## Short Sword
}

## This signal indicates that the pawn died. This is emitted AFTER the pawn has
## been removed from the scene and the corpse has been spawned in, but BEFORE it
## has been queued for freeing. Most useful for debugging and tests.
signal pawn_died

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Variable Declarations
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## Two of our *Position Algorithm*s, *Navigation* and *Detour Mesh*, require
## nodes in order to work. This configurable allows the user to provide the
## appropriate node. This is the node that will be used to check our current
## position and generate paths.
@export var navigation: NodePath
## The navigation [code]NodePath[/code] is resolved into this variable.
var navigation_node

## This pawn will inevitably need to drop items. When it drops these items, what
## node should these items be placed under?
@export var item_parent: NodePath
## The item parent [code]NodePath[/code] is resolved into this variable.
var item_parent_node

## The [code]BasePawn[/code] has two sprites - the [i]Visual Sprite[/i] and the
## [i]Weapon Sprite[/i]. We set these independently so any sprite of the same
## silhouette can reuse a weapon set. The whole system is designed for palette
## swaps.
##
## Anyway, you can specify the equipment that the pawn should be carrying by
## using this configurable.
@export var equipment: Equipment = Equipment.NONE:
    set = assert_equipment

## The current orientation/heading/direction of the pawn. Set to one of the
## [i]Cardinal[/i] [code]Enum[/code] values. Good for reading, but should
## generally only be set using feedback from an AI machine of some kind. Should
## be set using the [code]update_orient_enum[/code] function.
var _orient_enum = SOUTH

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Godot Processing - _ready, _process, etc.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# Called when the node enters the scene tree for the first time.
func _ready():
    # Get the navigation node
    navigation_node = get_node(navigation)

    # Get the item parent node
    item_parent_node = get_node(item_parent)

    # IF we have a pathing interface...
    if navigation_node != null:
        # Pass our navigation input down to the Level Interface Core. Calling
        # get_path() on the resolved node will get the absolute path for the
        # scene, so that we can ensure the LevelInterfaceCore is configured
        # correctly
        $LevelInterfaceCore.navigation_node = navigation_node.get_path()

    # IF we have a parent node to drop items on...
    if item_parent_node != null:
        # Pass our "item drop" input down to the Level Interface Core. Calling
        # get_path() on the resolved node will get the absolute path for the
        # scene, so that we can ensure the LevelInterfaceCore is configured
        # correctly
        $LevelInterfaceCore.item_parent_node = item_parent_node.get_path()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Setters & Util
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Sets the equipment of the sprite given an [code]Equipment[/code]
## [code]Enum[/code]. This includes loading the appropriate sprite frames.
func assert_equipment(new_equip):
    # Assign the enum
    equipment = new_equip

    # Set the appropriate frames
    match equipment:
        Equipment.NONE:
            $WeaponSprite.frames = null
        Equipment.SHORT_SWORD:
            $WeaponSprite.frames = short_sword_frames

    # The weapon sprite has to match the visual sprite, so let's just take the
    # animation and the current frame.
    $WeaponSprite.animation = $VisualSprite.animation
    $WeaponSprite.frame = $VisualSprite.frame


# Calculates the appropriate orientation enumeration given a vector.
func calculate_orient_enum(orient_vec: Vector3):
    # Return value goes here
    var ret_val
    # Move the X and Z fields into a Vector2 so we can easily calculate the
    # sprite's current angular direction. Note that the Z is actually
    # inverted; This makes our angles operate on a CCW turn (like a unit
    # circle)
    var orient_angle = Vector2(orient_vec.x, -orient_vec.z).angle()
    # Angles are, by default, in radian form. Good for computers, bad for
    # humans. Since we're just going to check values, and not do any
    # calculations or manipulations, let's just convert it to degrees!
    orient_angle = rad_to_deg(orient_angle)
    # Both of our sprites start off at 45 degrees on y - so to calculate the
    # true movement angle, we need rotate BACK 45 degrees.
    orient_angle -= 45
    # Godot doesn't do angles as 0 to 360 degress, it does -180 to 180. This
    # makes case-checks harder, so let's just shift things forward by 360 if
    # we're negative.
    if orient_angle < 0:
        orient_angle += 360

    # Right! Now we've got an appropriate angle - we just need to set the
    # zones. Each eigth-cardinal is allotted 45 degrees, centered around
    # increments of 45 - i.e. EAST is at 0, NOR_EAST at 45, NORTH at 90. The
    # periphery of the zone extends +- 45/2, or 22.5. Ergo, our zone checks
    # are values coming from 22.5 + (45 * i)
    if orient_angle < 22.5:
        ret_val = EAST
    elif orient_angle < 67.5:
        ret_val = NOR_EAST
    elif orient_angle < 112.5:
        ret_val = NORTH
    elif orient_angle < 157.5:
        ret_val = NOR_WEST
    elif orient_angle < 202.5:
        ret_val = WEST
    elif orient_angle < 247.5:
        ret_val = SOU_WEST
    elif orient_angle < 292.5:
        ret_val = SOUTH
    elif orient_angle < 337.5:
        ret_val = SOU_EAST
    else:
        ret_val = EAST

    return ret_val


## Updates the [code]_orient_enum[/code] variable given a [code]Vector3[/code].
## Sets the value to one of the [i]Cardinal[/i] [code]Enum[/code] values.
func update_orient_enum(orient_vec: Vector3):
    _orient_enum = calculate_orient_enum(orient_vec)


## Causes this item to take the specified amount of damage, of the specified
## type. When the item takes too much damage, it "dies" and removes itself from
## the scene.
func take_damage(amount, type = null):
    $CharacterStatsCore.take_damage(amount, type)


func _on_CharacterStatsCore_object_died(final_damage_type):
    var spin_base = DEATH_SPIN_MAXIMUM - DEATH_SPIN_MINIMUM
    var spin_add = DEATH_SPIN_MINIMUM

    var corpse = corpse_scene.instantiate()
    corpse.global_transform.origin = self.global_transform.origin
    corpse.rotation_degrees = Vector3(0, 45, 0)
    corpse.angular_velocity.x = ((randf() * spin_base) + spin_add)
    corpse.angular_velocity.y = ((randf() * spin_base) + spin_add)
    corpse.angular_velocity.z = ((randf() * spin_base) + spin_add)

    # If the level interface core actually has an item node...
    if $LevelInterfaceCore.item_node != null:
        # Stick the corpse under that
        $LevelInterfaceCore.item_node.add_child(corpse)
    # Otherwise...
    else:
        # Make the corpse a sibling of this node
        get_parent().add_child(corpse)

    # Wake up the corpse so it falls over
    corpse.sleeping = false

    # Now, remove ourselves from the scene
    get_parent().remove_child(self)

    # Now that we're not in the scene, tell everyone we died
    self.emit_signal("pawn_died")

    # Set ourselves up to be deleted
    queue_free()

# Pawn
The pawn is the main NPC of the game, and is one of the key areas of development.

One thing of interent - the *Pawn Sizer* scene. This scene is literally just a
scene with the 3D version that the 2D sprites are based on. This can be used for
reference, but was mostly used to scale the sprites until they were the
"correct" size.

# Must be tool for configuration checking. Interestingly, if this script isn't
# a tool, it can mess with the BaseActor's configuration warnings
@tool
class_name Actor
extends BaseActor

# TODO: How do we handle an Actor's alignment? i.e. how do we classify actors
# with respect to other actors

## An actor that can interact with the world and other actors
##
## The Actor is meant to be any active, living, moving entity in our theater
## paradigm. Any sort of character, or possibly even inanimate objects given
## life. Anything that people would normally term AI.
## [br][br]
## This class implements the interface provided by the [TheaterPiece] class.

#
# Signal Declarations
#
# signal sample_signal(value)

#
# Enum Declarations
#
# enum ExampleEnum { A = 0, B = 3.5, }

#
# Constant Declarations
#
# const VALUE = 5

#
# Export Variables
#
# @export var export_value  = ""

#
# Public Variables
#
# var public_var  = 0

#
# Private Variables
#
# var _private_var : bool = false

#
# Public onready Variables
#
# @onready var also_public_var  = getSomething()

#
# Private onready Variables
#
# @onready var _also_private_var  = getSomethingPrivate()

#
# Public functions
#
# func something():
#     pass


#
# Private functions
#
# Called when the node enters the scene tree for the first time.
func _ready():
    pass  # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
    pass

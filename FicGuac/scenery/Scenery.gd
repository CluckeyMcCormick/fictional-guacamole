# Must be tool for configuration checking. Interestingly, if this script isn't
# a tool, it can mess with the BaseProp's configuration warnings
@tool
class_name Scenery
extends BaseScenery

## A piece of scenery that is generally inactive but can be interacted with
##
## Scenery is meant to be any static, (probably) inert entity in our theater
## paradigm. Stuff like walls, trees, pieces of furniture, maybe even rocks.
## Anything that would generally not move, yet we need to be breakable and
## reactive.
## [br][br]
## This class implements the interface provided by the [TheaterPiece] class.

#
# Signal Declarations
#
# signal sample_signal(value)

#
# Enum Declarations
#
# enum ExampleEnum { A = 0, B = 3.5, }

#
# Constant Declarations
#
# const VALUE = 5

#
# Export Variables
#
# @export var export_value  = ""

#
# Public Variables
#
# var public_var  = 0

#
# Private Variables
#
# var _private_var : bool = false

#
# Public onready Variables
#
# @onready var also_public_var  = getSomething()

#
# Private onready Variables
#
# @onready var _also_private_var  = getSomethingPrivate()

#
# Public functions
#
# func something():
#     pass


#
# Private functions
#
# Called when the node enters the scene tree for the first time.
func _ready():
    pass  # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
    pass

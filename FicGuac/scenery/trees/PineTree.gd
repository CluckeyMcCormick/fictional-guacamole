# Set this as a tool so that we see the trees change in the editor
@tool
extends StaticBody3D

# What type of tree is this?
enum TreeType { PINE_A, PINE_B, PINE_C }

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

@export var type: TreeType = TreeType.PINE_A:
    set(new_type):
        type = new_type
        _tree_refresh()


# Called when the node enters the scene tree for the first time.
func _ready():
    # Immediately set the tree type using our preset tree type and light level
    _tree_refresh()


func _tree_refresh():
    var sprite_string = ""

    if not has_node("ActualSprite") or not has_node("AltSprite"):
        return

    # Set the collision status and get
    match type:
        TreeType.PINE_A:
            sprite_string = "pine_fc_a"
        TreeType.PINE_B:
            sprite_string = "pine_fc_b"
        TreeType.PINE_C:
            sprite_string = "pine_fc_c"

    $ActualSprite.animation = sprite_string
    $AltSprite.animation = sprite_string + "_fade"


func _on_VisibilityNotifier_screen_entered():
    $ActualSprite.visible = true
    $AltSprite.visible = true


func _on_VisibilityNotifier_screen_exited():
    $ActualSprite.visible = false
    $AltSprite.visible = false

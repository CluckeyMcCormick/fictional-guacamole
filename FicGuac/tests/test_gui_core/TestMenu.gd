extends VBoxContainer

## Menu that loads a list of our tests for the user to select
##
## This is a GUI that lists all of the available/registered tests. The list of
## tests is populated from Manifest's TEST_SCENES dictionary. It emits a signal
## when the user has made a choice. Pretty basic overall.

# Our global manifest node that holds all of the tests paths in a dictionary.
@onready var MANIFEST = get_node("/root/Manifest")

# Signal emitted whenever this menu chooses a scene. Provides the resource path
# and the formatted human-readable name
signal scene_chosen(resource_path, scene_name)

# Is this scene currently enabled? We try to en/disable the scene so we don't
# accidentally switch scenes
var disabled = true:
    set = set_disabled


func _ready():
    # We need to update the Item List using our scene paths above
    for test_name in MANIFEST.TEST_SCENES.keys():
        $ItemList.add_item(test_name)


# Set the disabled status for this scene.
func set_disabled(new_bool):
    disabled = new_bool
    if disabled:
        $Button.disabled = true
    else:
        $Button.disabled = false


func _on_button_pressed():
    # Get the selected item(s)
    var selected = $ItemList.get_selected_items()

    # Back out if we don't have anything selected
    if selected.size() <= 0:
        return

    # selected_item is currently a PackedInt32Array - we only want the first item
    # selected, so let's just get the first value out of it
    selected = selected[0]

    # Now we have an index for an item in the ItemList, but we need to translate
    # that in to a string so we have a key for the dict.
    selected = $ItemList.get_item_text(selected)

    # Now that we have a key, we can translate that into a resource path. Nab
    # that and emit it.
    emit_signal("scene_chosen", MANIFEST.TEST_SCENES[selected], selected)

extends CenterContainer

## Shell for running arbitrary test scenes
##
## The Test Shell provides a common GUI to all over our tests, using a mix of
## existing GUI components and the Test Menu. This scene gives us a single
## launching point for all tests. It loads and spawns the scene on-the-fly. It
## even provides a loading screen. It also provides a pause menu (complete with
## ACTUAL pausing!) to all scenes.
## [br][br]
## It also provides a convenient fourth item: a wonderful demonstration on how
## to do the other items using XSM! Seriously, the states have made everything
## WAY more clear.

# What is our currently running scene?
var scene_running = null
# What is the resource path for that scene?
var scene_res = null


func _ready():
    # The Pause menu is disabled
    $PauseLayer/PauseMenu.disabled = true

extends VBoxContainer

## GUI for directly manipulating the engine's speed
##
## The Time GUI was originally developed so that the interactions/happenings
## of one particular test could be more easily observed. However, this proved
## to be a really useful piece of functionality so we isolated it into this
## scene.
## [br][br]
## This scene consists of a couple of labels and a slider. It was designed to
## occupy the entire bottom 1/5 of the screen or so, but thanks to Godot's
## Control Nodes, it can fit just about anywhere.
## [br][br]
## Moving the slider will set the Engine's speed (e.g. the time flow) to the
## specified value. When this scene exits the tree, it resets the Engine speed
## (so no need to worry about that). Possible values range from 0 (stopped) to
## 1.5 (1 and 1/2 speed).

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Signal Handlers
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


func _on_Slider_value_changed(value):
    $TimeScale/Value.text = str(value)
    Engine.set_time_scale(value)


func _on_TimeGUI_tree_exiting():
    # Reset the time scale so that other things don't get messed up by this GUI
    # nonsense.
    Engine.set_time_scale(1.0)

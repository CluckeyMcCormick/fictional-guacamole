@tool
extends State


# This function is called when the state enters
# XSM enters the root first, the the children
func _on_enter(_args):
    var shell = target
    var menu = shell.get_node("TestMenu")
    # Assert menu state
    menu.disabled = false
    menu.visible = true

    # Menu state doesn't have any scene loaded or loading.
    shell.scene_res = null
    # If we happen to have a scene, free it.
    if shell.scene_running != null:
        shell.remove_child(shell.scene_running)
        shell.scene_running.queue_free()
    # Now blank the scene running anyway
    shell.scene_running = null

    var err = menu.scene_chosen.connect(_on_menu_scene_chosen)
    assert(err == OK, "Failed to connect to test menu!")


# This function is called when the State exits
# XSM before_exits the children first, then the root
func _on_exit(_args):
    var shell = target
    var menu = shell.get_node("TestMenu")

    # Assert the menu is disabled
    menu.disabled = true
    # Might as well hide it while we're at it
    menu.visible = false

    # We don't care if the user is picking a scene anymore
    menu.scene_chosen.disconnect(_on_menu_scene_chosen)


func _on_menu_scene_chosen(resource_path, _scene_name):
    var shell = target
    var menu = shell.get_node("TestMenu")

    # Disable the menu so the user doesn't accidentally trigger another load
    menu.disabled = true
    # Update the scene we're gonna load
    shell.scene_res = resource_path
    # Change to the loading scene!
    change_state("Loading")

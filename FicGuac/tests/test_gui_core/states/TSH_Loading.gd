@tool
extends State


# This function is called when the state enters
# XSM enters the root first, the the children
func _on_enter(_args):
    var shell = target
    var load_screen: Control = shell.get_node("LoadingLayer/LoadingScreen")

    # Connect the signals
    load_screen.loading_complete.connect(_on_loading_complete)
    load_screen.loading_failed.connect(_on_loading_failed)

    # Assert loading state
    load_screen.visible = true
    load_screen.initiate_scene_load(target.scene_res)


# This function is called when the State exits
# XSM before_exits the children first, then the root
func _on_exit(_args):
    var shell = target
    var load_screen: Control = shell.get_node("LoadingLayer/LoadingScreen")

    # Reset the loading screen
    load_screen.reset()
    load_screen.visible = false

    # We're done with these signals
    load_screen.loading_complete.disconnect(_on_loading_complete)
    load_screen.loading_failed.disconnect(_on_loading_failed)


func _on_loading_complete(scene_resource, path):
    # Tell user
    print("Successfully loaded ", path)
    # Instance the scene and put it in the tree
    target.scene_running = scene_resource.instantiate()
    target.add_child(target.scene_running)
    # We're going into the scene running state!
    change_state("SceneRunning")


func _on_loading_failed(path):
    # Tell user
    print("Load failed for path: ", path)
    # We're back at the menu now
    change_state("AtMenu")

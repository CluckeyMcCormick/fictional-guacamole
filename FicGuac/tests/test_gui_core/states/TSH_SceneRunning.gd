@tool
extends State


# Need ready to disable calls to _input that could be premature
func _ready():
    self.set_process_input(false)


# This function is called when the state enters
# XSM enters the root first, the the children
func _on_enter(_args):
    self.set_process_input(true)


# This function is called when the State exits
# XSM before_exits the children first, then the root
func _on_exit(_args):
    self.set_process_input(false)


func _input(event):
    # If the user presses the pause button...
    if event.is_action_pressed("game_core_pause"):
        # Change to the "ScenePaused" state!
        change_state("ScenePaused")

@tool
extends State


# This function is called when the state enters
# XSM enters the root first, the the children
func _on_enter(_args):
    var shell = target
    var pause_menu = shell.get_node("PauseLayer/PauseMenu")

    # Assert pause state
    pause_menu.visible = true
    pause_menu.disabled = false
    print("Game Paused!")
    # Pause the game
    get_tree().paused = true

    # Connect listening functions
    pause_menu.resume_game.connect(_on_menu_resume_game)
    pause_menu.main_menu.connect(_on_menu_main_menu)
    pause_menu.exit_game.connect(_on_menu_exit_game)


# This function is called when the State exits
# XSM before_exits the children first, then the root
func _on_exit(_args):
    var shell = target
    var pause_menu = shell.get_node("PauseLayer/PauseMenu")

    # Revert the pause state
    pause_menu.visible = false
    pause_menu.disabled = true
    # Unpause the game
    get_tree().paused = false

    # Disconnect listening functions
    pause_menu.resume_game.disconnect(_on_menu_resume_game)
    pause_menu.main_menu.disconnect(_on_menu_main_menu)
    pause_menu.exit_game.disconnect(_on_menu_exit_game)


func _on_menu_resume_game():
    var shell = target
    var pause_menu = shell.get_node("PauseLayer/PauseMenu")

    change_state("SceneRunning")
    # Disable the menu ahead of time to prevent double-dipping
    pause_menu.disabled = true
    # Have to unpause because state relies on _physics_process to change states
    get_tree().paused = false


func _on_menu_main_menu():
    var shell = target
    var pause_menu = shell.get_node("PauseLayer/PauseMenu")

    change_state("AtMenu")
    # Disable the menu ahead of time to prevent double-dipping
    pause_menu.disabled = true
    # Have to unpause because state relies on _physics_process to change states
    get_tree().paused = false


func _on_menu_exit_game():
    # Tell the user
    print("Quitting the game!")
    # Quit!
    get_tree().quit()

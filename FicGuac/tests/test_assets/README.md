# Tests Assets
This is a directory for scenes and assets that are used in testing -
exclusively for testing. Anything used sporadically or in level
construction/prototyping should go in the *utility* directory.

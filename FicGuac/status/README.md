# Status
This directory is for resources, scenes, and scripts associated with
object/motion AI status - health, being on fire, etc. It's something that
doesn't quite belong to the Motion AI, nor does it belong to objects. It's
really a category all it's own.

## Directories

### Conditions
Home directory for status conditions, which are nodes derived from the
[BaseStatusCondition] node.

class_name StatModBaseScale
extends "res://status/StatMod.gd"

# TODO: Restructure this as a Resource. Maybe? I just want this to appear in
# the GUI as something we can add to an Array.

## StatMod instance that scales according to given field
##
## This [StatMod]-derived class adds to a target stat the product of a given
## value and another (not necessarily different) base stat. For example, if the
## target stat is [code]max_hits[/code], and the base stat is
## [code]hit_potential[/code], and the modifier is [code].10[/code], then the
## result would be [code]max_hits += hit_potential * .10[/code]. This is meant
## for percentage gains, like 23% gain or loss to some stat. Again, it could be
## the same stat for both base and target, though this is discouraged.

## The string name of the field we derive scale-based calculations from. This
## two-field system exists to ensure modifiers are not stacked; or rather, that
## they are only stacked in the appropriate way.
## [br][br]
## If we used only one field for scaling operations, then applying this mod
## ahead of or after a [StatModBaseScale] would have two completely different
## results.
var base_var = ""


## This overrides the [method StatMod._init] function. This is what gets called
## when building the class - i.e. when calling [method StatModBaseScale.new].
func _init(new_target_var: String, new_base_var: String, new_mod_value: float):
    target_var = new_target_var
    mod_value = new_mod_value
    base_var = new_base_var


## This function overwrites the [method StatMod.apply] function. The scalar
## argument is for scaling-up-or-down the value-to-be-added. This is primarily
## used for when a status stacks multiple times.
func apply(core, scalar: float):
    var target_value = core.get(target_var)
    var add_value = core.get(base_var)
    add_value *= mod_value
    core.set(target_var, target_value - _applied_value + (add_value * scalar))
    _applied_value = add_value * scalar

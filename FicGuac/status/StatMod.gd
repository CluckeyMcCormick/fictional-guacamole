class_name StatMod

# TODO: Restructure this as a Resource. Maybe? I just want this to appear in
# the GUI as something we can add to an Array.

## Class for describing a stat modification
##
## An individual [StatMod] instance defines a single modification against a
## stat. The different types of [StatMod] classes have different behaviors -
## i.e. they modify values using different calculations. This is the base
## [StatMod] class, and defines a modifier that just adds a given value to
## stat. This value is not interpreted in any extra way, it's just
## straight-added.

## The string name of the field we're targeting. In other words, it's the
## actual field that is BEING dynamically modified. Using a string allows us to
## dynamically apply the modifiers - for example, we can check if a status core
## actually has a variable before applying the status effect. This means the
## game won't crash, even if an invalid side effect gets on an object.
var target_var: String = ""
## This is the actual modification value - the -.25 or +23 or whatever you need
## it to be. Remember this value will be added to field given by
## [member target_var].
var mod_value: float = 0
# The value that was previously applied to target_var. This allows each StatMod
# to clean up after itself. Ergo, it's recommended that you don't mess with
# this.
var _applied_value: float = 0


## Initialization function - this is what gets called when building the class
## - i.e. when calling [method StatMod.new].
func _init(new_target_var: String, new_mod_value: float):
    target_var = new_target_var
    mod_value = new_mod_value


## This function takes in a [CommonStatsCore] (or derivative/inheriting class)
## and a scalar. It applies the stat change to the core (unapplying any
## previous changes made by this modifier). The scalar is there if, for
## whatever reason, the [member mod_value] needs to be scaled - this is
## intended for stacking status effects.
func apply(core, scalar: float):
    var target_value = core.get(target_var)
    core.set(target_var, target_value - _applied_value + (mod_value * scalar))
    _applied_value = mod_value * scalar


## This function takes in a [CommonStatsCore] (or derivative/inheriting class)
## and removes the previously applied stat change. Keep in mind that this is
## tracked as dumbly as possible - the modifier just assumes the given core had
## the change applied and reverses it. Resets our internal tracking of the
## applied value to 0.
func unapply(core):
    var target_value = core.get(target_var)
    core.set(target_var, target_value - _applied_value)
    _applied_value = 0

extends Node3D

## A class for describing a status condition
##
## Sometimes, you have to modify some of the stats temporarily, or apply damage
## at a fixed interval, or so-on-and-so-on. That's where status effects come
## in! All status effects are separate scenes, and this class is the base
## class/scene that they all derive from. This base class defines the structure
## of the status conditions, while the actual content is defined in the derived
## status condition scenes.
## [br][br]
## Each status condition has both a series of modifiers and scalable particle
## effects. Preconstructed particle effects can also be added into derived
## status condition scenes - the status condition is a spatial node centered on
## the status core. You can thus translate, scale, and rotate the
## preconstructed particle effect as you see fit.

## The script that outlines the [StatMod] class. Loading it here will make it
## available to any derived classes for the purpose of creating new stat
## modifications.
const STAT_MOD_SCRIPT = preload("res://status/StatMod.gd")
## The script that outlines the [StatModBaseScale] class. Loading it here will
## make it available to any derived classes for the purpose of creating new
## stat modifications.
const STAT_MOD_BASE_SCALE_SCRIPT = preload("res://status/StatModBaseScale.gd")

# TODO: Renovate this class so the StatMods are create as an array of resources
# and the particles are children of the deriving condition scenes.

# TODO: This should be a class.

# TODO: Move timers configuration out of the scene and only spawn them if
# needed? Could be more clear when creating new status conditions...

## Does this condition have a limited lifespan - i.e. does it persist for a
## fixed amount of time before naturally dissipating? Defaults to true.
## [br][br]
## If true, the lifetime is specified by the "wait time" field on the
## lifetime timer node - this timer is automatically started once the condition
## enters the scene. Once the timer expires, the [signal condition_expired]
## signal fires.
## [br][br]
## The "wait time" field should be adjusted appropriately on the lifetime timer
## node in each deriving scene.
@export var limited_lifespan: bool = true

## Does this condition deal damage over time - i.e. does the condition deal a
## fixed amount of damage at fixed intervals for the duration of it's existence?
## [br][br]
## If true, the damage interval is specified by the "wait time" field on the
## damage interval timer node - this timer is automatically started once the
## condition enters the scene, and restarted once it completes. Every time the
## timer expires, the [signal dot_damaged] signal is fired.
## [br][br]
## The "wait time" field should be adjusted appropriately int the
## damage interval timer in each deriving scene.
@export var damage_over_time: bool = false

## This variable is the damage applied at every damage-over-time (DOT)
## interval.
@export var dot_damage: int = 0

## The icon used to represent this status effect. Though currently unused, we
## may use it for some sort of health-bar or other status-display.
@export var icon: Texture2D

## When we add a status condition to the [CommonStatsCore] (or a deriving
## scene), we register status effect using a unique name. We also remove the
## status effect using this unique name. This should be unique against all
## status effects in the game.
@export var keyname: String

# TODO: Why did I call this "human_name"? Call it "display_name"!

## This is the name we actually display to users; i.e. the PROPER name for this
## status effect
@export var human_name: String

## Emitted when the lifetime timer expires/finishes/fires. Indicates
## that the status effect has run it's course and should be removed. Emits with
## the corresponding status effect.
signal condition_expired(condition)
## Emitted when the damage-over-time interval has fired. Indicates that the
## status effect is dealing damage. Emits with the corresponding status effect
## and the damage value.
signal dot_damaged(condition, damage: int)

# Safety/control variable: have we already spawned in our different particle
# effects? This will prevent us from accidentally spawning too many particle
# systems.
var _particles_spawned = false


#
# Overload/Prototype Functions
#
## No comment (to be deleted)
func get_modifiers():
    pass


## No comment (to be deleted)
func get_scalable_particles():
    pass


#
# Utility/Initializer Functions
#
## No comment (to be deleted)
func spawn_particles(system_scale = Vector3(1, 1, 1)):
    # A new particle effects node that we're gonna add to the scene tree.
    var fx_node

    # If we've already spawned our particles, back out
    if _particles_spawned:
        return

    # Now we need to add the scalable/dynamic particle effects.
    for particle_item in get_scalable_particles():
        pass
        # Create the Scalable Particle Emitter
        #fx_node = SRPS.instantiate()
        # Pass it the blueprint
        #fx_node.set_blueprint( particle_item )
        # Scale the emitter
        #fx_node.scale_emitter( system_scale )
        # Stick the new system under this status effect.
        #add_child(fx_node)

    # We have now spawned the particle systems for this node
    _particles_spawned = true


#
# Base Functions
#
func _ready():
    # First, let's ensure that none of the timers are running unnecessarily
    $LifetimeTimer.autostart = false
    $DamageIntervalTimer.autostart = false

    # Alright - now, if this status condition has a lifetime, start the lifetime
    # timer.
    if limited_lifespan:
        $LifetimeTimer.start()
    # Otherwise, just make sure it's not running.
    else:
        $LifetimeTimer.stop()

    # If this status condition deals damage over time, start the timer.
    if damage_over_time:
        $DamageIntervalTimer.start()
    # Otherwise, just make sure it's not running.
    else:
        $DamageIntervalTimer.stop()


# When the lifetime timer expires, fire the "condition_expired" signal.
func _on_LifetimeTimer_timeout():
    # Stop the DamageIntervalTimer, so that we don't give damage after expiring
    $DamageIntervalTimer.stop()
    # Stop the LifetimeTimer too - this should only ever go off once.
    $LifetimeTimer.stop()
    # Fire the expiration signal
    emit_signal("condition_expired", self)


# When the dots timer expires, fire the "dot_damage" signal.
func _on_DamageIntervalTimer_timeout():
    # Fire the damage signal
    emit_signal("dot_damaged", self, dot_damage)
    # Start the timer over - no such thing as one-shot DOTS here.
    $DamageIntervalTimer.start()

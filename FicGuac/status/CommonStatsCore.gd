extends Marker3D

## A core for all theater components to take damage and have their stats
## modified
##
## The Common Stats Core is the primary status component for our damage-taking
## physics objects. This includes Motion AI, but also Items, or possibly even
## Props or Scenery. It is the singular reference point for status (i.e.
## health points). It was designed as a core becuase it was originally meant for
## use purely with Motion AI. However, I realized that a whole lot more than
## just Motion AI needed to take damage, and we'd need a common way to handle
## that EVERYWHERE.
## [br][br]
## However, because it needs to be common to everything, this node is
## purposefully very vague - it's meant to be inherited by other scenes so that
## we can create more specific status cores as needed.
## [br][br]
## Also, we call it a "stat" core because the status concept extends to what we
## would commonly call a character's stats - stuff like move speed, for
## example.
## [br][br]
## This core divides the stats into "base" stats - what the stats should be
## initially - and the "current" or "effective" status.

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Constant Declarations
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# This scene allows us to spawn little messages in the world, as needed. We'll
# use this to show damage values.
const _FLOAT_AWAY_TEXT = preload("res://special_effects/FloatAwayText.tscn")

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Variable Declarations
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## The base hitpoints for the object. This currently acts as both the default
## amount of health and a cap.
@export var base_hp: int = 50
## Controls whether we spawn damage floats whenever this core takes damage.
## Damage floats are little floating pieces of text, implemented via the Float
## Away Text scene. It helps us gauge how much damage was done, and where the
## damage was done.
@export var damage_floats: bool = true

## The current hitpoints for the object. This is not modified by buffs or
## debuffs; it is just a measure of health. This can be modified directly but
## it is recommended that you use the provided functions.
var curr_hp: int = base_hp
## A boolean value indicating whether the core is currently dead or not. This
## should not be altered directly. Yes, we even consider props and scenery to be
## "dead". Dead is, generally, considered an unrecoverable state.
var dead: bool = false

# Our different, ongoing status effects; the keys are each ongoing status
# condition's keyname, while the values are the effect nodes themselves.
var _active_effects: Dictionary = {}

## This signal indicates that the core has taken enough damage that it's HP has
## hit zero, and it has died.
signal object_died(final_damage_type)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Utility Functions
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Takes in a Base Status Condition derived/inherited class and applies it to
## the core.
func add_status_effect(status_effect):
    # These are used in value modifier calculations.
    var target_value
    var add_value

    # If we already have this status effect, skip it!
    if status_effect.keyname in _active_effects:
        return

    # Okay, first we're gonna add in the status effect.
    _active_effects[status_effect.keyname] = status_effect

    # Connect the status effect's expiration and damage-over-time signals to the
    # common core's functions.
    status_effect.connect(
        "condition_expired", Callable(self, "_on_condition_expire")
    )
    status_effect.connect(
        "dot_damaged", Callable(self, "_on_condition_dot_damaged")
    )

    # Spawn in the particle effects
    status_effect.spawn_particles()

    # Now we need to apply the modifiers.
    for mod in status_effect.get_modifiers():
        mod.apply(self, 1)

    # Finally, attach it as a child of this Stats Core node
    self.add_child(status_effect)


## Takes in a string and removes the corresponding status effect. The string
## should correspond to a Base Status Condition's keyname field, which is how
## we track the ongoing status conditions.
func remove_status_effect(status_keyname: String):
    # What's the status effect we retrieved?
    var sfx

    # If this status effect is not present, back out!
    if not status_keyname in _active_effects:
        return

    # Get the status effect
    sfx = _active_effects[status_keyname]
    # Remove the status effect from the array.
    _active_effects.erase(status_keyname)
    # Remove the status effect from the scene
    self.remove_child(sfx)
    # For each modifier in this status effect, unapply it
    for mod in sfx.get_modifiers():
        mod.unapply(self)
    # Finally, delete the status effect
    sfx.queue_free()


## Removes all the status effects from this core.
func clear_status_effects():
    # What's the status effect we retrieved?
    var sfx

    # For each keyname, remove the associated status effect.
    for keyname in _active_effects.keys():
        # Get the status effect
        sfx = _active_effects[keyname]
        # Remove the status effect from the array.
        _active_effects.erase(keyname)
        # Remove the status effect from the scene
        self.remove_child(sfx)
        # For each modifier in this status effect, unapply it
        for mod in sfx.get_modifiers():
            mod.unapply(self)
        # Finally, delete the status effect
        sfx.queue_free()


## Takes in an amount of damage and a damage type, dealing the specified amount
## of damage to the core (allowing for different resistances, of course). Emits
## the [signal object_died] signal if the core bottoms out on hitpoints. Also
## spawns damage floats if that's enabled.
## [br][br]
## The resulting curr HP value is clamped between 0 and base HP.
## [br][br]
## This should always be used when dealing damage to the core.
## [br][br]
## The damage will not be dealt if the core is already dead.
func take_damage(damage: int, damage_type = null):
    # If we're dead, we don't take damage. No coming back from that one.
    if dead:
        return

    # Set the HP
    curr_hp -= damage

    # If we're showing damage floats, then
    if damage_floats:
        var dmg_float = _FLOAT_AWAY_TEXT.instantiate()
        dmg_float.text = str(damage)
        # We'll assume that this core is always attached to a parent and,
        # furthermore, that parent is underneath a relatively constant node.
        get_parent().get_parent().add_child(dmg_float)
        # We will also assume our parent is some sort of Node3D (3D) node
        dmg_float.global_transform.origin = get_parent().global_transform.origin

    # Clamp it
    curr_hp = clamp(curr_hp, 0, base_hp)

    # If we don't have any health...
    if curr_hp == 0:
        # Then we're dead.
        dead = true
        # Tell the world that we're dead.
        emit_signal("object_died", damage_type)


## Takes in an amount of damage to heal, healing the core for that amount. This
## does not spawn damage floats.
## [br][br]
## The resulting curr hp value is clamped between 0 and the base HP.
## [br][br]
## No damage will be healed if the core is already dead.
## [br][br]
## This should always be used when healing damage on the core.
func heal_damage(damage_healed: int):
    # If we're dead, we don't heal damage. No coming back from that one.
    if dead:
        return

    # Set the HP
    curr_hp += damage_healed

    # Clamp it
    curr_hp = clamp(curr_hp, 0, base_hp)


func _on_condition_expire(condition):
    remove_status_effect(condition.keyname)


func _on_condition_dot_damaged(_condition, damage):
    take_damage(damage)

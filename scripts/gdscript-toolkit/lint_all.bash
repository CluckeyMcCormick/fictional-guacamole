#!/usr/bin/env bash

if ! command -v gdlint &> /dev/null
then
    echo "gdlint command could not be found"
    exit
fi


godot_root=$(realpath -L $(dirname $0)/../../FicGuac)
cd $godot_root

file_listing=$(find . -type f -name "*.gd" -not -path "./addons*")

gdlint $file_listing
exit $?

#!/usr/bin/env bash

# If we don't have the format command, no reason to hang around! Back out
if ! command -v gdformat &> /dev/null
then
    echo "gdformat command could not be found"
    exit 1
fi

# Parse our arguments to see if we're running in diff-check mode or not
extra_args=""
for i in "$@"; do
  case $i in
    --diff-check)
      # Enable the diff, which will tell us what is wrong but not actually do
      # anything.
      extra_args="--diff"
      ;;
    *)
      ;;
  esac
done

# Function for converting a file's indentation to spaces
function expand_file {
    expand --initial --tabs=4 "$1" > ./EXPAND_OP &&
    mv ./EXPAND_OP "$1"
}

# Function for converting a file's indentation to spaces
function unexpand_file {
    unexpand --first-only --tabs=4 "$1" > ./UNEXPAND_OP &&
    mv ./UNEXPAND_OP "$1"
}

# Export those functions so we can use them
export -f expand_file
export -f unexpand_file

# Navigate to the Godot root
godot_root=$(realpath -L $(dirname $0)/../../FicGuac)
cd $godot_root

# Get all of the files
file_listing=$(find . -type f -name "*.gd" -not -path "./addons*" | sort)

# Convert everything to tabs
echo -e -n $file_listing |
xargs -I '{}' --delimiter=" " bash -c "unexpand_file '{}'"

# Perform the format
gdformat --line-length=80 $extra_args $file_listing

# Convert everything back to spaces
echo -e -n $file_listing |
xargs -I '{}' --delimiter=" " bash -c "expand_file '{}'"


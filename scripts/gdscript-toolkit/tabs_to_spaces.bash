#!/usr/bin/env bash

godot_root=$(realpath -L $(dirname $0)/../../FicGuac)
cd $godot_root

# Function for converting a file's indentation to spaces
function expand_file {
    expand --initial --tabs=4 "$1" > ./EXPAND_OP &&
    mv ./EXPAND_OP "$1"
}

# Export that function so we can use it
export -f expand_file

# Get all of the files
file_listing=$(find . -type f -name "*.gd" -not -path "./addons*" | sort)

# Convert everything to spaces
echo -e -n $file_listing |
xargs -I '{}' --delimiter=" " bash -c "expand_file '{}'"

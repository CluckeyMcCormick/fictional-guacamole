#!/usr/bin/env bash

if ! command -v gdradon &> /dev/null
then
    echo "gdradon command could not be found"
    exit
fi


godot_root=$(realpath -L $(dirname $0)/../../FicGuac)
cd $godot_root

file_listing=$(find . -type f -name "*.gd" -not -path "./addons*")

gdradon cc $file_listing
exit $?

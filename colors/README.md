# Colors

This game uses the Resurrect64 palette by
[Kerrie Lake](https://kerrielakeportfolio.wordpress.com/).
I retrieved the palette from
[Lospec](https://lospec.com/palette-list/resurrect-64).

I love restricted color palettes in any media, so I was very excited to restrict my assets to a particular palette. This stylization would also make everything more aesthetically pleasing (and a programmer such as myself needs as many easy aesthetic points as I can score).

The current palette has 64+ colors. I had experimented with other, more restrictive palettes, but I always found I was lacking a particular color I needed. Other palettes were 'cool', present a more gray or blue tone overall. I selected *Resurrect64* because it was a warmer palette that had a range of colors that aligned with my needs. The color tone-gradient-square-thingies also helped my artless brain A LOT!

I did have to extend the palette just for the buildings - I wanted the wood and stones to be a bit darker. You can see my extensions under the main palette in `resurrect64_extended.png`. I don't think I did that great, and I'm sure an actual artist could select colors that got the job done better, BUT they work for what I need (which is all I really need).

These colors have been included for reference AND for use with the ImageMagick
Posterize-Ditherize script. That script takes in an file that contains all the
colors we can map to. The "chits" are provided as convenient inputs for that
script.
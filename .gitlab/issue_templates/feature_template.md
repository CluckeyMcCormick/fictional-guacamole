### What's your vision for this feature?
Describe your vision for the feature.

### What problem will this feature solve?
What problem will this feature solve? Do you have any evidence for these problems?

### Any ideas on how to implement this idea?
How would you implement this idea? If you have any ideas, capture them here. Even a vague idea may have some merit!

### Describe the bug
A clear and concise description of what the bug is.

### To Reproduce
Steps to reproduce the bug.

### Expected behavior
A clear and concise description of what you expected to happen.

### Screenshots/Video
If applicable, add screenshots/video to help explain your problem.
Remember to mark the area in the game thats impacted.

### Desktop
 - OS: [e.g. Ubuntu, Windows 11, Windows 10, MacOS, etc.]
 - Resolution [e.g. 2560 X 1289]

### Additional context
Add any other context about the problem here.
